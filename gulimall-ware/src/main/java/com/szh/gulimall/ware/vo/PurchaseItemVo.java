package com.szh.gulimall.ware.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2022/11/15
 */
@Data
public class PurchaseItemVo {
    private Long itemId;
    private Integer status;
    private String reason;
}
