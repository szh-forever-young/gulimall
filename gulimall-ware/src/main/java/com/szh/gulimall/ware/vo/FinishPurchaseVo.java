package com.szh.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/11/15
 */
@Data
public class FinishPurchaseVo {
    private Long id; //采购单id
    private List<PurchaseItemVo> items; //每个采购需求
}
