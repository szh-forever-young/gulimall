package com.szh.gulimall.ware.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: SongZiHao
 * @date: 2023/2/3
 */
@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
