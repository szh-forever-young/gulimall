package com.szh.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.dto.mq.OrderTo;
import com.szh.common.dto.mq.StockLockTo;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.ware.entity.PurchaseDetailEntity;
import com.szh.gulimall.ware.entity.WareSkuEntity;
import com.szh.gulimall.ware.vo.LockStockResult;
import com.szh.gulimall.ware.vo.SkuHasStockVo;
import com.szh.gulimall.ware.vo.WareSkuLockVo;

import java.util.List;
import java.util.Map;

/**
 * 商品库存
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:10
 */
public interface WareSkuService extends IService<WareSkuEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 将采购成功的采购需求批量插入wms_ware_sku表
     */
    void saveOrUpdateWareSku(Long skuId, Long wareId, Integer skuNum);

    /**
     * 查询sku是否有库存
     */
    List<SkuHasStockVo> getHasSkuStock(List<Long> skuIds);

    /**
     * 锁定库存
     */
    Boolean orderLockStock(WareSkuLockVo vo);

    void unlockStock(StockLockTo to);

    void unlockStock(OrderTo orderTo);
}

