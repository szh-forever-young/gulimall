package com.szh.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/11/14
 */
@Data
public class MergePurchaseDetailVo {
    private Long purchaseId;
    private List<Long> items;
}
