package com.szh.gulimall.ware.dao;

import com.szh.gulimall.ware.entity.PurchaseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 采购信息
 * 
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:11
 */
@Mapper
public interface PurchaseDao extends BaseMapper<PurchaseEntity> {
	
}
