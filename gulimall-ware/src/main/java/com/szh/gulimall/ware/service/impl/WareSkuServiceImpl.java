package com.szh.gulimall.ware.service.impl;

import com.alibaba.fastjson.TypeReference;
import com.szh.common.dto.mq.OrderTo;
import com.szh.common.dto.mq.StockDetailTo;
import com.szh.common.dto.mq.StockLockTo;
import com.szh.common.exception.NoStockException;
import com.szh.common.utils.R;
import com.szh.gulimall.ware.entity.WareOrderTaskDetailEntity;
import com.szh.gulimall.ware.entity.WareOrderTaskEntity;
import com.szh.gulimall.ware.feign.OrderFeignService;
import com.szh.gulimall.ware.feign.ProductFeignService;
import com.szh.gulimall.ware.service.WareOrderTaskDetailService;
import com.szh.gulimall.ware.service.WareOrderTaskService;
import com.szh.gulimall.ware.vo.OrderItemVo;
import com.szh.gulimall.ware.vo.OrderVo;
import com.szh.gulimall.ware.vo.SkuHasStockVo;
import com.szh.gulimall.ware.vo.WareSkuLockVo;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.Query;
import com.szh.gulimall.ware.dao.WareSkuDao;
import com.szh.gulimall.ware.entity.WareSkuEntity;
import com.szh.gulimall.ware.service.WareSkuService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

@Service("wareSkuService")
public class WareSkuServiceImpl extends ServiceImpl<WareSkuDao, WareSkuEntity> implements WareSkuService {

    private static final Logger LOGGER = LoggerFactory.getLogger(WareSkuServiceImpl.class);

    @Autowired
    private WareSkuDao wareSkuDao;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private OrderFeignService orderFeignService;

    @Autowired
    private WareOrderTaskService wareOrderTaskService;

    @Autowired
    private WareOrderTaskDetailService wareOrderTaskDetailService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 条件查询商品库存列表
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();
        String skuId = (String) params.get("skuId");
        if (!StringUtils.isEmpty(skuId)) {
            queryWrapper.eq("sku_id", skuId);
        }
        String wareId = (String) params.get("wareId");
        if (!StringUtils.isEmpty(wareId)) {
            queryWrapper.eq("ware_id", wareId);
        }
        IPage<WareSkuEntity> page = this.page(
                new Query<WareSkuEntity>().getPage(params),
                queryWrapper
        );
        return new PageUtils(page);
    }

    /**
     * 将采购成功的采购需求批量插入wms_ware_sku表
     */
    @Override
    @Transactional
    public void saveOrUpdateWareSku(Long skuId, Long wareId, Integer skuNum) {
        QueryWrapper<WareSkuEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("sku_id", skuId).eq("ware_id", wareId);
        List<WareSkuEntity> wareSkuEntityList = baseMapper.selectList(queryWrapper);
        //远程调用商品服务，根据skuId查询skuName
        R result = productFeignService.info(skuId);
        Map<String, Object> data = (Map<String, Object>) result.get("skuInfo");
        String skuName = (String) data.get("skuName");
        if (result.getCode() == 0) {
            //判断表中是否有这个库存记录，如果没有，则是save新增库存操作
            if (CollectionUtils.isEmpty(wareSkuEntityList)) {
                WareSkuEntity wareSkuEntity = new WareSkuEntity();
                wareSkuEntity.setSkuId(skuId);
                wareSkuEntity.setSkuName(skuName);
                wareSkuEntity.setWareId(wareId);
                wareSkuEntity.setStock(skuNum);
                wareSkuEntity.setStockLocked(0);
                baseMapper.insert(wareSkuEntity);
            } else { //存在这条库存记录，则是update更新库存操作
                baseMapper.updateWareSkuInfo(skuId, skuName, wareId, skuNum);
            }
        }
    }

    /**
     * 查询sku是否有库存
     */
    @Override
    public List<SkuHasStockVo> getHasSkuStock(List<Long> skuIds) {
        List<SkuHasStockVo> vos = skuIds.stream().map(skuId -> {
            SkuHasStockVo vo = new SkuHasStockVo();
            Long count = baseMapper.getHasSkuStock(skuId);
            vo.setSkuId(skuId);
            vo.setHasStock(!Objects.isNull(count) && count != 0);
            return vo;
        }).collect(Collectors.toList());
        return vos;
    }

    /**
     * 锁定库存
     * 库存解锁的场景：
     *    1.下订单成功，订单过期没有支付被系统自动取消、被用户手动取消，此时要将对应的库存解锁
     *    2.下订单成功，库存锁定成功，后续其他的业务调用失败，导致订单回滚，此时要将对应的库存解锁
     */
    @Override
    @Transactional(rollbackFor = NoStockException.class)
    public Boolean orderLockStock(WareSkuLockVo vo) {
        //保存库存工作单的详情信息
        WareOrderTaskEntity wareOrderTaskEntity = new WareOrderTaskEntity();
        wareOrderTaskEntity.setOrderSn(vo.getOrderSn());
        wareOrderTaskService.save(wareOrderTaskEntity);
        //找到哪个商品在哪个仓库有库存
        List<OrderItemVo> locks = vo.getLocks();
        List<SkuWareHasStock> skuWareHasStockList = locks.stream().map(item -> {
            SkuWareHasStock stock = new SkuWareHasStock();
            Long skuId = item.getSkuId();
            stock.setSkuId(skuId);
            stock.setNum(item.getCount());
            List<Long> wareIds = wareSkuDao.listWareIdHasStock(skuId);
            stock.setWareId(wareIds);
            return stock;
        }).collect(Collectors.toList());
        //锁定库存
        for (SkuWareHasStock wareHasStock : skuWareHasStockList) {
            boolean skuStocked = false;
            Long skuId = wareHasStock.getSkuId();
            List<Long> wareIds = wareHasStock.getWareId();
            if (CollectionUtils.isEmpty(wareIds)) {
                throw new NoStockException(skuId);
            }
            for (Long wareId : wareIds) {
                Long row = wareSkuDao.lockSkuStock(skuId, wareId, wareHasStock.getNum());
                if (row == 1) {
                    skuStocked = true;
                    WareOrderTaskDetailEntity detailEntity = new WareOrderTaskDetailEntity(null, skuId,
                            "", wareHasStock.getNum(), wareOrderTaskEntity.getId(), wareId, 1);
                    wareOrderTaskDetailService.save(detailEntity);
                    //库存锁定成功，向MQ中发送消息（当前商品锁定了几件的工作单记录）
                    StockLockTo stockLockTo = new StockLockTo();
                    stockLockTo.setId(wareOrderTaskEntity.getId());
                    StockDetailTo stockDetailTo = new StockDetailTo();
                    BeanUtils.copyProperties(detailEntity, stockDetailTo);
                    stockLockTo.setDetail(stockDetailTo);
                    rabbitTemplate.convertAndSend("stock-event-exchange", "stock.locked", stockLockTo);
                    break;
                } else {

                }
            }
            if (!skuStocked) {
                throw new NoStockException(skuId);
            }
        }
        return true;
    }

    @Override
    @Transactional
    public void unlockStock(StockLockTo to) {
        StockDetailTo detail = to.getDetail();
        Long detailId = detail.getId();
        //查询库存工作单详情表中是否有这条锁定库存的记录
        WareOrderTaskDetailEntity detailEntity = wareOrderTaskDetailService.getById(detailId);
        if (Objects.nonNull(detailEntity)) { //有，再看订单状态，没有这个订单，必须解锁；有这个订单，如果超时未支付，则必须解锁，如果按时支付，则不能解锁
            Long id = to.getId();
            WareOrderTaskEntity orderTaskEntity = wareOrderTaskService.getById(id);
            String orderSn = orderTaskEntity.getOrderSn();
            //根据订单号查询订单状态，只有订单不存在或者状态是超时未支付（已取消），才可以解锁库存
            R res = orderFeignService.getOrderStatus(orderSn);
            if (res.getCode() == 0) {
                OrderVo orderVo = res.getData(new TypeReference<OrderVo>(){});
                //订单不存在或者订单状态是已取消
                if (Objects.isNull(orderVo) || orderVo.getStatus() == 4) {
                    if (detailEntity.getLockStatus() == 1) {
                        //将库存解锁
                        wareSkuDao.unlockStock(detail.getSkuId(), detail.getWareId(), detail.getSkuNum());
                        //更新库存工作单的状态为已解锁
                        WareOrderTaskDetailEntity entity = new WareOrderTaskDetailEntity();
                        entity.setId(detailId);
                        entity.setLockStatus(2);
                        wareOrderTaskDetailService.updateById(entity);
                    }
                }
            } else {
                throw new RuntimeException("远程调用失败....");
            }
        } else { //没有，说明库存锁定失败了，会进行事务回滚，此情况无需解锁，但是要手动确认消息，将死信队列中的消息删掉
            //channel.basicAck(message.getMessageProperties().getDeliveryTag(), false);
        }
    }

    @Override
    @Transactional
    public void unlockStock(OrderTo orderTo) {
        String orderSn = orderTo.getOrderSn();
        //查询最新的库存状态，防止重复解锁库存
        WareOrderTaskEntity taskEntity = wareOrderTaskService.getOrderTaskByOrderSn(orderSn);
        Long taskId = taskEntity.getId();
        //根据库存工作单id查询所有没有解锁的库存，进行解锁
        QueryWrapper<WareOrderTaskDetailEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("task_id", taskId);
        queryWrapper.eq("lock_status", 1);
        List<WareOrderTaskDetailEntity> detailEntityList = wareOrderTaskDetailService.list(queryWrapper);
        for (WareOrderTaskDetailEntity entity : detailEntityList) {
            //将库存解锁
            wareSkuDao.unlockStock(entity.getSkuId(), entity.getWareId(), entity.getSkuNum());
            //更新库存工作单的状态为已解锁
            WareOrderTaskDetailEntity detailEntity = new WareOrderTaskDetailEntity();
            detailEntity.setId(entity.getId());
            detailEntity.setLockStatus(2);
            wareOrderTaskDetailService.updateById(detailEntity);
        }
    }

    @Data
    static class SkuWareHasStock {
        private Long skuId;
        private Integer num;
        private List<Long> wareId;
    }
}