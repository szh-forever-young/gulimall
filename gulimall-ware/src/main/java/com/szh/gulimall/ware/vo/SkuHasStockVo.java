package com.szh.gulimall.ware.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
@Data
public class SkuHasStockVo {
    private Long skuId;
    private Boolean hasStock;
}
