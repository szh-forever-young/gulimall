package com.szh.gulimall.ware.vo;

import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
@Data
public class WareSkuLockVo {
    private String orderSn;
    private List<OrderItemVo> locks;
}
