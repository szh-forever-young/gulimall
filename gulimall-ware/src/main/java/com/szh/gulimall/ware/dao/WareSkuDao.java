package com.szh.gulimall.ware.dao;

import com.szh.gulimall.ware.entity.PurchaseDetailEntity;
import com.szh.gulimall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 商品库存
 * 
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:10
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {

    void updateWareSkuInfo(@Param("skuId") Long skuId,
                           @Param("skuName") String skuName,
                           @Param("wareId") Long wareId,
                           @Param("skuNum") Integer skuNum);

    Long getHasSkuStock(@Param("skuId") Long skuId);

    List<Long> listWareIdHasStock(@Param("skuId") Long skuId);

    Long lockSkuStock(@Param("skuId") Long skuId,
                      @Param("wareId") Long wareId,
                      @Param("num") Integer num);

    void unlockStock(@Param("skuId") Long skuId,
                     @Param("wareId") Long wareId,
                     @Param("skuNum") Integer skuNum);
}
