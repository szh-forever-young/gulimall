package com.szh.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.ware.entity.PurchaseDetailEntity;

import java.util.List;
import java.util.Map;

/**
 * 
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:11
 */
public interface PurchaseDetailService extends IService<PurchaseDetailEntity> {

    /**
     * 条件查询采购需求列表
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 根据每个采购单id查出它对应的所有采购需求
     */
    List<PurchaseDetailEntity> listByPurchaseId(Long id);
}

