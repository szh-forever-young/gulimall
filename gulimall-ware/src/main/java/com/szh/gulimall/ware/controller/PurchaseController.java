package com.szh.gulimall.ware.controller;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import com.szh.gulimall.ware.vo.FinishPurchaseVo;
import com.szh.gulimall.ware.vo.MergePurchaseDetailVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.ware.entity.PurchaseEntity;
import com.szh.gulimall.ware.service.PurchaseService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 采购信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:11
 */
@RefreshScope
@RestController
@RequestMapping("/ware/purchase")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    /**
     * 查询所有未领取的采购单
     */
    @GetMapping("/unreceive/list")
    public R queryUnreceivePurchaseList(@RequestParam Map<String, Object> params) {
        PageUtils page = purchaseService.queryUnreceivePurchaseList(params);
        return R.ok().put("page", page);
    }

    /**
     * 将多个采购需求合并为一个采购单
     */
    @PostMapping("/merge")
    public R mergePurchaseDetail(@RequestBody MergePurchaseDetailVo purchaseDetailVo) {
        purchaseService.mergePurchaseDetail(purchaseDetailVo);
        return R.ok();
    }

    /**
     * 采购人员领取采购单
     * 不属于后台管理系统接口，属于采购人员在自己手机app中调用生效
     */
    @PostMapping("/received")
    public R received(@RequestBody List<Long> ids) {
        purchaseService.received(ids);
        return R.ok();
    }

    /**
     * 完成采购
     */
    @PostMapping("/done")
    public R done(@RequestBody FinishPurchaseVo finishPurchaseVo) {
        purchaseService.done(finishPurchaseVo);
        return R.ok();
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = purchaseService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
		PurchaseEntity purchase = purchaseService.getById(id);
        return R.ok().put("purchase", purchase);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody PurchaseEntity purchase) {
        purchase.setCreateTime(new Date());
        purchase.setUpdateTime(new Date());
		purchaseService.save(purchase);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody PurchaseEntity purchase) {
		purchaseService.updateById(purchase);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
		purchaseService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }
}
