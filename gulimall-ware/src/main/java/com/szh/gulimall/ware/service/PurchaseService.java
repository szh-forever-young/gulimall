package com.szh.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.ware.entity.PurchaseEntity;
import com.szh.gulimall.ware.vo.FinishPurchaseVo;
import com.szh.gulimall.ware.vo.MergePurchaseDetailVo;

import java.util.List;
import java.util.Map;

/**
 * 采购信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:11
 */
public interface PurchaseService extends IService<PurchaseEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询所有未领取的采购单
     */
    PageUtils queryUnreceivePurchaseList(Map<String, Object> params);

    /**
     * 将多个采购需求合并为一个采购单
     */
    void mergePurchaseDetail(MergePurchaseDetailVo purchaseDetailVo);

    /**
     * 采购人员领取采购单
     * 不属于后台管理系统接口，属于采购人员在自己手机app中调用生效
     */
    void received(List<Long> ids);

    /**
     * 完成采购
     */
    void done(FinishPurchaseVo finishPurchaseVo);
}

