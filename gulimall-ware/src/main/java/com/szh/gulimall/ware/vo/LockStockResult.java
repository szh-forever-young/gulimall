package com.szh.gulimall.ware.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
@Data
public class LockStockResult {
    private Long skuId;
    private Integer num;
    private Boolean locked;
}
