package com.szh.gulimall.ware.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.ware.entity.WareInfoEntity;
import com.szh.gulimall.ware.vo.FareVo;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 仓库信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:52:10
 */
public interface WareInfoService extends IService<WareInfoEntity> {

    /**
     * 条件查询仓库列表，带分页展示
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取运费信息
     */
    FareVo getFare(Long addrId);
}

