package com.szh.gulimall.search.vo;

import com.szh.common.dto.es.SkuEsModel;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 商城页面根据指定条件检索之后，返回给前端页面的数据模型
 *
 * @author: SongZiHao
 * @date: 2022/12/25
 */
@Data
public class SearchResult {

    /**
     * 商品上架存入ES的数据模型，检索之后返回的也应该是这个数据类型
     */
    private List<SkuEsModel> products;

    /**
     * 当前页码
     */
    private Integer pageNum;

    private List<Integer> pageNavs;

    /**
     * 总记录数
     */
    private Long total;

    /**
     * 总页码
     */
    private Integer totalPages;

    /**
     * 查询出的结果涉及到的分类信息列表
     */
    private List<CatalogVo> catalogs;

    /**
     * 查询出的结果涉及到的品牌信息列表
     */
    private List<BrandVo> brands;

    /**
     * 查询出的结果涉及到的属性信息列表
     */
    private List<AttrVo> attrs;

    /**
     * 面包屑导航
     */
    private List<NavVo> navs = new ArrayList<>();

    /**
     * 已经被筛选的属性id集合
     */
    private List<Long> attrIds = new ArrayList<>();

    @Data
    public static class CatalogVo {
        /**
         * 分类id
         */
        private Long catalogId;

        /**
         * 分类名称
         */
        private String catalogName;
    }

    @Data
    public static class BrandVo {
        /**
         * 品牌id
         */
        private Long brandId;

        /**
         * 品牌名称
         */
        private String brandName;

        /**
         * 品牌图片
         */
        private String brandImg;
    }

    @Data
    public static class AttrVo {
        /**
         * 属性id
         */
        private Long attrId;

        /**
         * 属性名称
         */
        private String attrName;

        /**
         * 属性值，每个属性可以选择多个值，比如分辨率属性：可以多选为：高清、超清
         */
        private List<String> attrValue;
    }

    @Data
    public static class NavVo {
        /**
         * 导航属性名称
         */
        private String navName;

        /**
         * 导航属性具体值
         */
        private String navValue;

        /**
         * 取消某个导航属性值之后，要跳转到的链接地址
         */
        private String link;
    }
}
