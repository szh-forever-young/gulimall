package com.szh.gulimall.search.vo;

import lombok.Data;

import java.util.List;

/**
 * 封装页面所有可能传来的检索条件
 * catalog3Id=225 & keyword=小米 & sort=saleCount_asc & hasStock=0/1 &
 * skuPrice=_6000 & brandId=1 & brandId=2 & attrs=1_5寸:6寸 & attrs=2_系统:安卓
 *
 * @author: SongZiHao
 * @date: 2022/12/25
 */
@Data
public class SearchParam {

    /**
     * 页面传来的全文匹配关键字
     */
    private String keyword;

    /**
     * 三级分类id
     */
    private Long catalog3Id;

    /**
     * 排序条件
     * saleCount_asc：按照saleCount升序
     * saleCount_desc：按照saleCount降序
     */
    private String sort;

    /**
     * 是否只显示有库存，默认有库存
     * 0表示无库存，1表示有库存
     */
    private Integer hasStock;

    /**
     * 价格区间
     * 1到500：1_500
     * 500以内：_500
     * 500以上：500_
     */
    private String skuPrice;

    /**
     * 品牌id列表，可以多选
     */
    private List<Long> brandId;

    /**
     * 属性列表，可以多选
     */
    private List<String> attrs;

    /**
     * 页码
     */
    private Integer pageNum = 1;

    /**
     * 原生的所有查询条件
     */
    private String queryString;
}
