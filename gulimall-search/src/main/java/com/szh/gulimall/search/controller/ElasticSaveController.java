package com.szh.gulimall.search.controller;

import com.szh.common.dto.es.SkuEsModel;
import com.szh.common.constant.GulimallCodeEnum;
import com.szh.common.utils.R;
import com.szh.gulimall.search.service.ProductSaveService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * 功能：商品上架，将上架的商品保存到ES中
 *
 * @author: SongZiHao
 * @date: 2022/12/18
 */
@RestController
@RequestMapping("/search")
public class ElasticSaveController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ElasticSaveController.class);

    @Autowired
    private ProductSaveService productSaveService;

    /**
     * 商品上架
     */
    @PostMapping("/productUp")
    public R productUp(@RequestBody List<SkuEsModel> skuEsModels) {
        boolean flag = false;
        try {
            flag = productSaveService.productUp(skuEsModels);
        } catch (IOException ex) {
            LOGGER.error("商品上架异常：{}", ex.getMessage());
        }
        if (!flag) {
            return R.ok();
        } else {
            return R.error(GulimallCodeEnum.PRODUCT_UP_EXCEPTION.getCode(), GulimallCodeEnum.PRODUCT_UP_EXCEPTION.getMsg());
        }
    }
}
