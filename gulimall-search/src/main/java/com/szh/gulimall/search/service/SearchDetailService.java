package com.szh.gulimall.search.service;

import com.szh.gulimall.search.vo.SearchParam;
import com.szh.gulimall.search.vo.SearchResult;

/**
 * @author: SongZiHao
 * @date: 2022/12/25
 */
public interface SearchDetailService {

    /**
     * 根据参数param，检索出指定的商品详情信息
     */
    SearchResult search(SearchParam param);
}
