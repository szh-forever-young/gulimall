package com.szh.gulimall.search.controller;

import com.szh.gulimall.search.service.SearchDetailService;
import com.szh.gulimall.search.vo.SearchParam;
import com.szh.gulimall.search.vo.SearchResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * 功能：搜索商品跳转到商品的详情页
 *
 * @author: SongZiHao
 * @date: 2022/12/25
 */
@Controller
public class SearchDetailController {

    @Autowired
    private SearchDetailService searchDetailService;

    /**
     * 根据参数param，检索出指定的商品详情信息
     */
    @GetMapping("/list.html")
    public String listPage(SearchParam param, Model model, HttpServletRequest request) {
        param.setQueryString(request.getQueryString());
        SearchResult result = searchDetailService.search(param);
        model.addAttribute("result", result);
        return "list";
    }
}
