package com.szh.gulimall.search.constant;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
public class ElasticSearchConstant {

    /**
     * sku保存在ES中的索引
     */
    public static final String PRODUCT_INDEX = "gulimall_product";

    /**
     * 页码
     */
    public static final Integer PRODUCT_PAGE_SIZE = 8;
}
