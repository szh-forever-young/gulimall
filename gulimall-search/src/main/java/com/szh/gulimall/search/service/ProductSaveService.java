package com.szh.gulimall.search.service;

import com.szh.common.dto.es.SkuEsModel;

import java.io.IOException;
import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
public interface ProductSaveService {

    /**
     * 商品上架
     */
    boolean productUp(List<SkuEsModel> skuEsModels) throws IOException;
}
