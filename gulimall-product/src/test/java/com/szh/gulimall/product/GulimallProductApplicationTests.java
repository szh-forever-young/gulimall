package com.szh.gulimall.product;

import com.szh.gulimall.product.dao.AttrGroupDao;
import com.szh.gulimall.product.dao.SkuSaleAttrValueDao;
import com.szh.gulimall.product.service.BrandService;

import com.szh.gulimall.product.service.CategoryService;
import com.szh.gulimall.product.vo.SkuItemSaleAttrVo;
import com.szh.gulimall.product.vo.SkuItemVo;
import com.szh.gulimall.product.vo.SpuItemAttrGroupVo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;


@RunWith(SpringRunner.class)
@SpringBootTest
public class GulimallProductApplicationTests {

    @Autowired
    private BrandService brandService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private RedissonClient redissonClient;

    @Autowired
    private AttrGroupDao attrGroupDao;

    @Autowired
    private SkuSaleAttrValueDao skuSaleAttrValueDao;

    @Test
    public void test() {
        Long[] catelogPath = categoryService.queryCatelogPath(225L);
        System.out.println(Arrays.asList(catelogPath));
    }

    @Test
    public void testRedis() {
        String uuid = UUID.randomUUID().toString();
        redisTemplate.opsForValue().set("hello", "value_" + uuid);
        String value = redisTemplate.opsForValue().get("hello");
        System.out.println(value);
    }

    @Test
    public void testRedissonClient() {
        System.out.println(redissonClient);
    }

    @Test
    public void testAttrGroupDao() {
        List<SpuItemAttrGroupVo> result = attrGroupDao.getAttrGroupWithAttrsBySpuId(1L, 225L);
        System.out.println(result);
    }

    @Test
    public void testSkuSaleAttrValueDao() {
        List<SkuItemSaleAttrVo> result = skuSaleAttrValueDao.getSaleAttrsBySpuId(1L);
        System.out.println(result);
    }
}
