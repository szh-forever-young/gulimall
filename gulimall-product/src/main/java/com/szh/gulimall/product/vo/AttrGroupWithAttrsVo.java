package com.szh.gulimall.product.vo;

import com.szh.gulimall.product.entity.AttrEntity;
import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/11/12
 */
@Data
public class AttrGroupWithAttrsVo {
    /**
     * 分组id
     */
    private Long attrGroupId;
    /**
     * 组名
     */
    private String attrGroupName;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 描述
     */
    private String descript;
    /**
     * 组图标
     */
    private String icon;
    /**
     * 所属分类id
     */
    private Long catelogId;

    private List<AttrEntity> attrs;
}
