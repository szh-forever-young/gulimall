package com.szh.gulimall.product.service.impl;

import com.szh.gulimall.product.dao.AttrAttrgroupRelationDao;
import com.szh.gulimall.product.dao.AttrDao;
import com.szh.gulimall.product.entity.AttrEntity;
import com.szh.gulimall.product.service.AttrService;
import com.szh.gulimall.product.vo.AttrGroupWithAttrsVo;
import com.szh.gulimall.product.vo.SkuItemVo;
import com.szh.gulimall.product.vo.SpuItemAttrGroupVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.Query;

import com.szh.gulimall.product.dao.AttrGroupDao;
import com.szh.gulimall.product.entity.AttrGroupEntity;
import com.szh.gulimall.product.service.AttrGroupService;
import org.springframework.util.StringUtils;

@Service("attrGroupService")
public class AttrGroupServiceImpl extends ServiceImpl<AttrGroupDao, AttrGroupEntity> implements AttrGroupService {

    @Autowired
    private AttrAttrgroupRelationDao relationDao;

    @Autowired
    private AttrDao attrDao;

    @Autowired
    private AttrService attrService;

    /**
     * 普通分页查询
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<AttrGroupEntity> page = this.page(
                new Query<AttrGroupEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    /**
     * 获取某个三级分类下的所有属性分组列表
     */
    @Override
    public PageUtils queryPage(Map<String, Object> params, Long categoryId) {
        String key = (String) params.get("key");
        QueryWrapper<AttrGroupEntity> queryWrapper = new QueryWrapper<>();
        //拼接条件查询参数
        //select * from pms_attr_group where catelog_id=? and (attr_group_id=key or attr_group_name like %key%)
        if (!StringUtils.isEmpty(key)) {
            queryWrapper.and(obj -> obj.eq("attr_group_id", key).or().like("attr_group_name", key));
        }
        //如果分类id是0，则查询全部的属性分组信息
        if (categoryId == 0) {
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryWrapper
            );
            return new PageUtils(page);
        } else { //如果分类id不为0，则查询对应分类id下的所有属性分组信息
            queryWrapper.eq("catelog_id", categoryId);
            IPage<AttrGroupEntity> page = this.page(
                    new Query<AttrGroupEntity>().getPage(params),
                    queryWrapper
            );
            return new PageUtils(page);
        }
    }

    /**
     * 获取某个分类下的所有属性分组中的所有属性（规格参数/基本属性）
     */
    @Override
    public List<AttrGroupWithAttrsVo> queryAttrGroupWithAttrsByCatelogId(Long catelogId) {
        //1.获取当前分类下的所有属性分组
        QueryWrapper<AttrGroupEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("catelog_id", catelogId);
        List<AttrGroupEntity> groupEntityList = this.list(queryWrapper);
        //2.获取这些属性分组中的所有属性
//        List<Long> groupIds = groupEntityList.stream().map(AttrGroupEntity::getAttrGroupId).collect(Collectors.toList());
//        QueryWrapper<AttrAttrgroupRelationEntity> wrapper = new QueryWrapper<>();
//        wrapper.in("attr_group_id", groupIds);
//        List<AttrAttrgroupRelationEntity> relationEntityList = relationDao.selectList(wrapper);
//        Map<Long, Long> attrMap = relationEntityList.stream().collect(Collectors.toMap(AttrAttrgroupRelationEntity::getAttrGroupId, AttrAttrgroupRelationEntity::getAttrId));
//        List<Long> attrIds = relationEntityList.stream().map(AttrAttrgroupRelationEntity::getAttrId).collect(Collectors.toList());
//        List<AttrEntity> attrEntityList = attrDao.selectBatchIds(attrIds);
        List<AttrGroupWithAttrsVo> attrsVoList = groupEntityList.stream().map(group -> {
            AttrGroupWithAttrsVo attrsVo = new AttrGroupWithAttrsVo();
            BeanUtils.copyProperties(group, attrsVo);
            List<AttrEntity> attrEntityList = attrService.queryAttrRelation(attrsVo.getAttrGroupId());
            attrsVo.setAttrs(attrEntityList);
            return attrsVo;
        }).filter(item -> Objects.nonNull(item.getAttrs())).collect(Collectors.toList());
        return attrsVoList;
    }

    /**
     * 根据spuId、catalogId查询当前spu所有的属性分组信息（属性分组名、每个分组中的属性名、属性值）
     */
    @Override
    public List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId) {
        return baseMapper.getAttrGroupWithAttrsBySpuId(spuId, catalogId);
    }

}