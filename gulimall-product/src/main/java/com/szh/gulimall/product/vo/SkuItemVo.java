package com.szh.gulimall.product.vo;

import com.szh.gulimall.product.entity.SkuImagesEntity;
import com.szh.gulimall.product.entity.SkuInfoEntity;
import com.szh.gulimall.product.entity.SpuInfoDescEntity;
import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/1/4
 */
@Data
public class SkuItemVo {
    //sku基本信息 pms_sku_info
    private SkuInfoEntity skuInfoEntity;

    //sku图片信息 pms_sku_images
    private List<SkuImagesEntity> skuImagesEntityList;

    //spu的销售属性组合信息
    private List<SkuItemSaleAttrVo> skuItemSaleAttrList;

    //spu的基本信息 pms_spu_info_desc
    private SpuInfoDescEntity spuInfoDescEntity;

    //spu的规格参数信息
    private List<SpuItemAttrGroupVo> spuItemAttrGroupList;

    //是否有库存
    private boolean hasStock = true;
}
