package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.AttrEntity;
import com.szh.gulimall.product.vo.AttrGroupRelationVo;
import com.szh.gulimall.product.vo.AttrRespVo;
import com.szh.gulimall.product.vo.AttrVo;

import java.util.List;
import java.util.Map;

/**
 * 商品属性
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface AttrService extends IService<AttrEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存属性与属性分组的关联关系
     */
    void saveAttr(AttrVo attr);

    /**
     * 获取某个分类下的所有属性信息
     * attrType: 属性类型, 0-销售属性, 1-规格参数
     */
    PageUtils queryBaseAttrList(Map<String, Object> params, Long catelogId, String type);

    /**
     * 根据属性id查询详细信息，同时设置分组信息、分类全路径
     */
    AttrRespVo queryAttrInfo(Long attrId);

    /**
     * 修改属性信息，同时修改属性分组的关联信息
     */
    void updateAttr(AttrVo attr);

    /**
     * 根据属性分组id查询当前属性分组关联的所有属性信息（规格参数/基本属性）
     */
    List<AttrEntity> queryAttrRelation(Long attrGroupId);

    /**
     * 根据属性分组id查询当前属性分组没有被其他分组关联的所有属性信息（规格参数/基本属性）
     */
    PageUtils queryNoAttrRelation(Map<String, Object> params, Long attrGroupId);

    /**
     * 删除属性分组的关联关系，只删除关联关系，不删除属性
     */
    void deleteAttrRelationBatch(AttrGroupRelationVo[] vos);

    /**
     * 在指定的属性集合中查询出可以被检索的那些属性id
     */
    List<Long> selectSearchAttrIds(List<Long> attrIds);
}

