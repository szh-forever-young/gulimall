package com.szh.gulimall.product.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Catelog2Vo { //二级分类vo类
    private String catalog1Id; //父分类id，也即一级分类id
    private List<Category3Vo> catalog3List;
    private String id;
    private String name;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Category3Vo { //三级分类vo类
        private String catalog2Id; //父分类id，也即二级分类id
        private String id;
        private String name;
    }
}
