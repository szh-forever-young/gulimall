package com.szh.gulimall.product.feign;

import com.szh.common.dto.SkuReductionDto;
import com.szh.common.dto.SpuBoundsDto;
import com.szh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author: SongZiHao
 * @date: 2022/11/13
 */
@FeignClient("gulimall-coupon")
public interface CouponFeignService {

    //5.保存spu的积分信息 sms_spu_bounds
    @PostMapping("/coupon/spubounds/save")
    R saveSpuBounds(@RequestBody SpuBoundsDto spuBoundsDto);

    //6.4 保存sku的优惠、满减等信息 sms_sku_ladder sms_sku_full_reduction sms_member_price
    @PostMapping("/coupon/skufullreduction/saveSkuReduction")
    R saveSkuReduction(@RequestBody SkuReductionDto skuReductionDto);
}
