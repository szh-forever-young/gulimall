package com.szh.gulimall.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/1/4
 */
@Data
public class SpuItemAttrGroupVo {
    private String groupName;
    private List<Attr> attrGroupList;
}
