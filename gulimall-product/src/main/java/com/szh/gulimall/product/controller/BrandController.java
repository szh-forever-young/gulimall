package com.szh.gulimall.product.controller;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.szh.common.valid.AddGroup;
import com.szh.common.valid.UpdateGroup;
import com.szh.common.valid.UpdateStatusGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.product.entity.BrandEntity;
import com.szh.gulimall.product.service.BrandService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 品牌
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@RefreshScope
@RestController
@RequestMapping("/product/brand")
public class BrandController {

    @Autowired
    private BrandService brandService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = brandService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{brandId}")
    public R info(@PathVariable("brandId") Long brandId) {
		BrandEntity brand = brandService.getById(brandId);
        return R.ok().put("brand", brand);
    }

    /**
     * 根据id集合批量查询品牌信息
     */
    @GetMapping("/listByBrandIds")
    public R listByBrandIds(@RequestParam("brandIds") List<Long> brandIds) {
        List<BrandEntity> brandList = brandService.listByBrandIds(brandIds);
        return R.ok().put("brandList", brandList);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@Validated(value = {AddGroup.class}) @RequestBody BrandEntity brand) {
		brandService.save(brand);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@Validated(value = {UpdateGroup.class}) @RequestBody BrandEntity brand) {
		brandService.updateDetail(brand);
        return R.ok();
    }

    /**
     * 修改品牌状态
     */
    @PostMapping("/updateStatus")
    public R updateStatus(@Validated(value = {UpdateStatusGroup.class}) @RequestBody BrandEntity brandEntity) {
        brandService.updateById(brandEntity);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] brandIds) {
		brandService.removeByIds(Arrays.asList(brandIds));
        return R.ok();
    }
}
