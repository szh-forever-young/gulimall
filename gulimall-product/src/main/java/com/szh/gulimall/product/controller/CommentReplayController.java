package com.szh.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.product.entity.CommentReplayEntity;
import com.szh.gulimall.product.service.CommentReplayService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 商品评价回复关系
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@RefreshScope
@RestController
@RequestMapping("/product/commentreplay")
public class CommentReplayController {

    @Autowired
    private CommentReplayService commentReplayService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = commentReplayService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
		CommentReplayEntity commentReplay = commentReplayService.getById(id);
        return R.ok().put("commentReplay", commentReplay);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody CommentReplayEntity commentReplay) {
		commentReplayService.save(commentReplay);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody CommentReplayEntity commentReplay) {
		commentReplayService.updateById(commentReplay);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
		commentReplayService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }
}
