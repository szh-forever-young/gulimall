package com.szh.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.product.entity.CategoryEntity;
import com.szh.gulimall.product.service.CategoryService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 商品三级分类
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@RefreshScope
@RestController
@RequestMapping("/product/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    /**
     * 查询所有分类及子分类，以树形结构组装成List集合
     */
    @GetMapping("/list/tree")
    public R listWithTree() {
        List<CategoryEntity> categoryEntityList = categoryService.listWithTree();
        return R.ok().put("data", categoryEntityList);
    }

    /**
     * 根据分类id查询分类信息
     */
    @GetMapping("/info/{catId}")
    public R info(@PathVariable("catId") Long catId) {
		CategoryEntity category = categoryService.getById(catId);
        return R.ok().put("data", category);
    }

    /**
     * 新增分类信息
     */
    @PostMapping("/save")
    public R save(@RequestBody CategoryEntity category) {
		categoryService.save(category);
        return R.ok();
    }

    /**
     * 单个修改
     */
    @PostMapping("/update")
    public R update(@RequestBody CategoryEntity category) {
		categoryService.updateDetail(category);
        return R.ok();
    }

    /**
     * 批量修改
     */
    @PostMapping("/updateBatch")
    public R updateBatch(@RequestBody CategoryEntity[] categoryEntities) {
        categoryService.updateBatchById(Arrays.asList(categoryEntities));
        return R.ok();
    }

    /**
     * 批量逻辑删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] catIds) {
		categoryService.removeCategoryByIds(Arrays.asList(catIds));
        return R.ok();
    }
}
