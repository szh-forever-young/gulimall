package com.szh.gulimall.product.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: SongZiHao
 * @date: 2022/11/12
 */
@Data
public class Bounds {
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}