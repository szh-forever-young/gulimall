package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.SpuInfoDescEntity;
import com.szh.gulimall.product.entity.SpuInfoEntity;
import com.szh.gulimall.product.vo.SpuSaveVo;

import java.util.Map;

/**
 * spu信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface SpuInfoService extends IService<SpuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 保存spu基本信息
     */
    void saveSpuInfo(SpuSaveVo spuSaveVo);

    //1.保存spu基本信息 pms_spu_info
    void saveBaseSpuInfo(SpuInfoEntity spuInfoEntity);

    /**
     * 条件查询spu列表
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 商品上架
     */
    void spuUp(Long spuId);

    /**
     * 根据skuId查询对应的spu信息
     */
    SpuInfoEntity getSpuInfoBySkuId(Long skuId);
}

