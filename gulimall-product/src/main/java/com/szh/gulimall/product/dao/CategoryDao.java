package com.szh.gulimall.product.dao;

import com.szh.gulimall.product.entity.CategoryEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品三级分类
 * 
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@Mapper
public interface CategoryDao extends BaseMapper<CategoryEntity> {
	
}
