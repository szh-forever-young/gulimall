package com.szh.gulimall.product.vo;

import lombok.Data;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/1/4
 */
@Data
public class SkuItemSaleAttrVo {
    private Long attrId;
    private String attrName;
    private List<AttrValueWithSkuIdVo> attrValues;
}
