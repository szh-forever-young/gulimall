package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.ProductAttrValueEntity;

import java.util.List;
import java.util.Map;

/**
 * spu属性值
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface ProductAttrValueService extends IService<ProductAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //4.保存spu的规格参数/基本属性 pms_product_attr_value
    void saveProductAttrValue(List<ProductAttrValueEntity> attrValueEntityList);

    /**
     * 查询某个spu商品的所有基本属性/规格参数
     */
    List<ProductAttrValueEntity> baseAttrListForSpu(Long spuId);

    /**
     * 批量更新当前spu商品下的所有基本属性/规格参数
     */
    void updateSpuAttrs(Long spuId, List<ProductAttrValueEntity> entityList);
}

