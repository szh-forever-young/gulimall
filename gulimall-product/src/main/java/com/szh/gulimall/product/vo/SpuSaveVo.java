package com.szh.gulimall.product.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 发布商品的总体VO封装类
 * 包括: Attr、BaseAttrs、Skus、Bounds、Images、MemberPrice
 *
 * @author: SongZiHao
 * @date: 2022/11/12
 */
@Data
public class SpuSaveVo {
    private String spuName;
    private String spuDescription;
    private Long catalogId;
    private Long brandId;
    private BigDecimal weight;
    private int publishStatus;
    private List<String> decript;
    private List<String> images;
    private Bounds bounds;
    private List<BaseAttrs> baseAttrs;
    private List<Skus> skus;
}