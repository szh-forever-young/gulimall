package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.SpuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * spu图片
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface SpuImagesService extends IService<SpuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //3.保存spu的图片集 pms_spu_images
    void saveSpuImages(Long id, List<String> imagesList);
}

