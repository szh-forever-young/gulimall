package com.szh.gulimall.product.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2022/11/12
 */
@Data
public class Images {
    private String imgUrl;
    private int defaultImg;
}