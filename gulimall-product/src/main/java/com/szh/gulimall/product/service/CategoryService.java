package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.CategoryEntity;
import com.szh.gulimall.product.vo.Catelog2Vo;

import java.util.List;
import java.util.Map;

/**
 * 商品三级分类
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface CategoryService extends IService<CategoryEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 查询所有分类及子分类，以树形结构组装起来
     */
    List<CategoryEntity> listWithTree();

    /**
     * 批量逻辑删除
     */
    void removeCategoryByIds(List<Long> catIds);

    /**
     * 找到categoryId的完整路径 [父/子/孙]
     */
    Long[] queryCatelogPath(Long categoryId);

    void updateDetail(CategoryEntity category);

    /**
     * 查询所有的一级分类
     */
    List<CategoryEntity> getLevel1Categorys();

    /**
     * 加载网站首页的所有菜单数据（这里用到了Redis的缓存、分布式锁Redisson）
     */
    Map<String, List<Catelog2Vo>> getCatalogJsonWithSpringCache();
}

