package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.AttrGroupEntity;
import com.szh.gulimall.product.vo.AttrGroupWithAttrsVo;
import com.szh.gulimall.product.vo.SkuItemVo;
import com.szh.gulimall.product.vo.SpuItemAttrGroupVo;

import java.util.List;
import java.util.Map;

/**
 * 属性分组
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface AttrGroupService extends IService<AttrGroupEntity> {

    /**
     * 普通分页查询
     */
    PageUtils queryPage(Map<String, Object> params);

    /**
     * 获取某个三级分类下的所有属性分组列表
     */
    PageUtils queryPage(Map<String, Object> params, Long categoryId);

    /**
     * 获取某个分类下的所有属性分组中的所有属性（规格参数/基本属性）
     */
    List<AttrGroupWithAttrsVo> queryAttrGroupWithAttrsByCatelogId(Long catelogId);

    /**
     * 根据spuId、catalogId查询当前spu所有的属性分组信息（属性分组名、每个分组中的属性名、属性值）
     */
    List<SpuItemAttrGroupVo> getAttrGroupWithAttrsBySpuId(Long spuId, Long catalogId);
}

