package com.szh.gulimall.product.web;

import com.szh.gulimall.product.entity.CategoryEntity;
import com.szh.gulimall.product.service.CategoryService;
import com.szh.gulimall.product.vo.Catelog2Vo;
import org.redisson.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
@Controller
public class IndexController {

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private RedissonClient redisson;

    @Autowired
    private StringRedisTemplate redisTemplate;

    /**
     * 查询所有的一级分类
     */
    @GetMapping(value = {"/", "/index.html"})
    public String indexPage(Model model) {
        //查询所有的一级分类
        List<CategoryEntity> categoryEntities = categoryService.getLevel1Categorys();
        model.addAttribute("categorys", categoryEntities);
        return "index";
    }

    /**
     * 加载网站首页的所有菜单数据（这里用到了Redis的缓存、分布式锁Redisson、SpringCache）
     */
    @ResponseBody
    @GetMapping("/index/catalog.json")
    public Map<String, List<Catelog2Vo>> getCatalogJsonWithRedis() {
        Map<String, List<Catelog2Vo>> map = categoryService.getCatalogJsonWithSpringCache();
        return map;
    }

    //------------- 以下均为测试Redisson的接口，与商城业务无关 -------------

    /**
     * 测试lock锁
     * lock()：未指定锁的过期时间，则使用默认的private long lockWatchdogTimeout = 30 * 1000; 30s会自动续期
     * lock(10, TimeUnit.SECONDS)：指定了锁的过期时间，则使用我们指定的10s，不会自动续期
     */
    @ResponseBody
    @GetMapping("/hello")
    public String hello() {
        //获取一把锁，只要锁的名字一样，就是同一把锁
        RLock lock = redisson.getLock("my-lock");
        //加锁，有一个线程拿到锁之后，其他线程会阻塞式等待，默认加锁30s
        //无参lock方法会存在锁的自动续期，如果业务超长，运行期间每隔10s会自动给锁续满30s，避免业务执行过程中锁过期被删除的情况
        //加锁的业务代码只要执行完毕，就不会再给当前锁续期，即使不手动解锁，Redisson默认在30s后自动解锁
        lock.lock();
//        lock.lock(10, TimeUnit.SECONDS); //该方法不会自动续期，10s后锁自动过期删除
        try {
            System.out.println(Thread.currentThread().getName() + "加锁成功，执行业务....");
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            //解锁
            System.out.println(Thread.currentThread().getName() + "解锁....");
            lock.unlock();
        }
        return "hello";
    }

    /**
     * 测试ReadWriteLock读写锁 - 写锁（排他锁）
     * 只要有写锁的存在，其他操作必然会阻塞等待
     */
    @ResponseBody
    @GetMapping("/write")
    public String write() {
        //依次获取读写锁、写锁
        RReadWriteLock rwLock = redisson.getReadWriteLock("rw-lock");
        RLock wLock = rwLock.writeLock();
        wLock.lock(); //加锁
        String value = "";
        try {
            System.out.println(Thread.currentThread().getId() + " 获取写锁成功....");
            value = UUID.randomUUID().toString();
            Thread.sleep(20000);
            redisTemplate.opsForValue().set("writeValue", value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            wLock.unlock(); //解锁
            System.out.println(Thread.currentThread().getId() + " 释放写锁....");
        }
        return value;
    }

    /**
     * 测试ReadWriteLock读写锁 - 读锁（共享锁）
     * 只要有写锁的存在，其他操作必然会阻塞等待
     */
    @ResponseBody
    @GetMapping("/read")
    public String read() {
        //依次获取读写锁、读锁
        RReadWriteLock rwLock = redisson.getReadWriteLock("rw-lock");
        RLock rLock = rwLock.readLock();
        rLock.lock(); //加锁
        String value = "";
        try {
            System.out.println(Thread.currentThread().getId() + " 获取读锁成功....");
            value = redisTemplate.opsForValue().get("writeValue");
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            rLock.unlock(); //解锁
            System.out.println(Thread.currentThread().getId() + " 释放读锁....");
        }
        return value;
    }

    /**
     * Semaphore信号量测试，模拟停车、取车场景
     */
    @ResponseBody
    @GetMapping("/park")
    public String park() throws InterruptedException {
        RSemaphore park = redisson.getSemaphore("park");
        //获取一个信号量，相当于占一个车位
        park.acquire();
        return "park";
    }

    /**
     * Semaphore信号量测试，模拟停车、取车场景
     */
    @ResponseBody
    @GetMapping("/go")
    public String go() {
        RSemaphore park = redisson.getSemaphore("park");
        //删除一个信号量，相当于取走一辆车，空出一个车位
        park.release();
        return "go";
    }

    /**
     * CountDownLatch闭锁测试，放假锁门，必须等待所有班级的人都走完，才可以锁大门
     */
    @ResponseBody
    @GetMapping("/lockDoor")
    public String lockDoor() throws InterruptedException {
        RCountDownLatch cdlLock = redisson.getCountDownLatch("door");
        //设置初始值为5，模拟一共5个班级
        cdlLock.trySetCount(5);
        //等待五个班级都走完，值变为0，代码才可以继续向下执行
        cdlLock.await();
        return "放假了....";
    }

    /**
     * CountDownLatch闭锁测试，放假锁门，必须等待所有班级的人都走完，才可以锁大门
     */
    @ResponseBody
    @GetMapping("/goHome/{id}")
    public String goHome(@PathVariable("id") Integer id) {
        RCountDownLatch cdlLock = redisson.getCountDownLatch("door");
        //值减1，即走完了一个班级
        cdlLock.countDown();
        return id + "班的人都走了....";
    }
}
