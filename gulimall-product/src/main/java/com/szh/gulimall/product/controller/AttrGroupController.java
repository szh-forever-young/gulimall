package com.szh.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.szh.gulimall.product.entity.AttrEntity;
import com.szh.gulimall.product.service.AttrAttrgroupRelationService;
import com.szh.gulimall.product.service.AttrService;
import com.szh.gulimall.product.service.CategoryService;
import com.szh.gulimall.product.vo.AttrGroupRelationVo;
import com.szh.gulimall.product.vo.AttrGroupWithAttrsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.product.entity.AttrGroupEntity;
import com.szh.gulimall.product.service.AttrGroupService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 属性分组
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@RefreshScope
@RestController
@RequestMapping("/product/attrgroup")
public class AttrGroupController {

    @Autowired
    private AttrGroupService attrGroupService;

    @Autowired
    private AttrService attrService;

    @Autowired
    private AttrAttrgroupRelationService relationService;

    @Autowired
    private CategoryService categoryService;

    /**
     * 获取某个三级分类下的所有属性分组列表
     */
    @GetMapping("/list/{categoryId}")
    public R listByCategoryId(@RequestParam Map<String, Object> params,
                              @PathVariable("categoryId") Long categoryId) {
        PageUtils page = attrGroupService.queryPage(params, categoryId);
        return R.ok().put("page", page);
    }

    /**
     * 根据属性分组id查询某个属性分组的信息
     */
    @GetMapping("/info/{attrGroupId}")
    public R info(@PathVariable("attrGroupId") Long attrGroupId) {
		AttrGroupEntity attrGroup = attrGroupService.getById(attrGroupId);
		Long categoryId = attrGroup.getCatelogId();
		Long[] path = categoryService.queryCatelogPath(categoryId);
		attrGroup.setCatelogPath(path);
        return R.ok().put("attrGroup", attrGroup);
    }

    /**
     * 根据属性分组id查询当前属性分组关联的所有属性信息（规格参数/基本属性）
     */
    @GetMapping("/{attrGroupId}/attr/relation")
    public R queryAttrRelation(@PathVariable("attrGroupId") Long attrGroupId) {
        List<AttrEntity> attrEntityList = attrService.queryAttrRelation(attrGroupId);
        return R.ok().put("data", attrEntityList);
    }

    /**
     * 根据属性分组id查询当前属性分组没有被其他分组关联的所有属性信息（规格参数/基本属性）
     */
    @GetMapping("/{attrGroupId}/noattr/relation")
    public R queryNoAttrRelation(@RequestParam Map<String, Object> params,
                                 @PathVariable("attrGroupId") Long attrGroupId) {
        PageUtils page = attrService.queryNoAttrRelation(params, attrGroupId);
        return R.ok().put("page", page);
    }

    /**
     * 获取某个分类下的所有属性分组中的所有属性（规格参数/基本属性）
     */
    @GetMapping("/{catelogId}/withattr")
    public R queryAttrGroupWithAttrsByCatelogId(@PathVariable("catelogId") Long catelogId) {
        List<AttrGroupWithAttrsVo> list = attrGroupService.queryAttrGroupWithAttrsByCatelogId(catelogId);
        return R.ok().put("data", list);
    }

    /**
     * 新增属性与分组的关联关系
     */
    @PostMapping("/attr/relation")
    public R saveAttrRelation(@RequestBody List<AttrGroupRelationVo> voList) {
        relationService.saveAttrRelation(voList);
        return R.ok();
    }

    /**
     * 保存某个属性分组的信息
     */
    @PostMapping("/save")
    public R save(@RequestBody AttrGroupEntity attrGroup) {
		attrGroupService.save(attrGroup);
        return R.ok();
    }

    /**
     * 修改某个属性分组的信息
     */
    @PostMapping("/update")
    public R update(@RequestBody AttrGroupEntity attrGroup) {
		attrGroupService.updateById(attrGroup);
        return R.ok();
    }

    /**
     * 删除属性分组的关联关系，只删除关联关系，不删除属性
     */
    @PostMapping("/attr/relation/delete")
    public R deleteAttrRelationBatch(@RequestBody AttrGroupRelationVo[] vos) {
        attrService.deleteAttrRelationBatch(vos);
        return R.ok();
    }

    /**
     * 删除某个属性分组的信息
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] attrGroupIds) {
		attrGroupService.removeByIds(Arrays.asList(attrGroupIds));
        return R.ok();
    }

}
