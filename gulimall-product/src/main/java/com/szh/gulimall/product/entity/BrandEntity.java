package com.szh.gulimall.product.entity;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;
import java.util.Date;

import com.szh.common.valid.AddGroup;
import com.szh.common.valid.ListValue;
import com.szh.common.valid.UpdateGroup;
import com.szh.common.valid.UpdateStatusGroup;
import lombok.Data;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;

/**
 * 品牌
 * 
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@Data
@TableName("pms_brand")
public class BrandEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 品牌id
	 */
	@TableId
	@Null(message = "新增不能指定品牌id", groups = {AddGroup.class})
	@NotNull(message = "修改必须指定品牌id", groups = {UpdateGroup.class})
	private Long brandId;

	/**
	 * 品牌名
	 */
	@NotBlank(message = "品牌名必须填写提交", groups = {AddGroup.class, UpdateGroup.class})
	private String name;

	/**
	 * 品牌logo地址
	 */
	@NotBlank(message = "logo必须是一个合法的url地址", groups = {AddGroup.class})
	@URL(message = "logo必须是一个合法的url地址", groups={AddGroup.class, UpdateGroup.class})
	private String logo;

	/**
	 * 介绍
	 */
	@NotBlank(message = "介绍信息必须填写提交", groups = {AddGroup.class, UpdateGroup.class})
	private String descript;

	/**
	 * 显示状态[0-不显示；1-显示]
	 */
	@NotNull(message = "显示状态不能为空", groups = {AddGroup.class, UpdateStatusGroup.class})
	@ListValue(values = {0, 1}, groups = {AddGroup.class, UpdateStatusGroup.class})
	private Integer showStatus;

	/**
	 * 检索首字母
	 */
	@NotEmpty(message = "检索首字母不能为空", groups = {AddGroup.class})
	@Pattern(regexp="^[a-zA-Z]$", message = "检索首字母必须是一个字母", groups={AddGroup.class, UpdateGroup.class})
	private String firstLetter;

	/**
	 * 排序
	 */
	@NotNull(message = "排序字段不能为空", groups = {AddGroup.class})
	@Min(value = 0, message = "排序字段必须大于等于0", groups = {AddGroup.class,UpdateGroup.class})
	private Integer sort;

}
