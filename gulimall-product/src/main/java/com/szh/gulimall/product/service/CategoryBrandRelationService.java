package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.BrandEntity;
import com.szh.gulimall.product.entity.CategoryBrandRelationEntity;

import java.util.List;
import java.util.Map;

/**
 * 品牌分类关联
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface CategoryBrandRelationService extends IService<CategoryBrandRelationEntity> {

    PageUtils queryPage(Map<String, Object> params);

    void saveDetail(CategoryBrandRelationEntity categoryBrandRelation);

    void updateBrandInfo(Long brandId, String name);

    void updateCategoryInfo(Long catId, String name);

    /**
     * 获取当前分类关联的所有品牌列表
     */
    List<BrandEntity> relationBrandsList(Long catId);
}

