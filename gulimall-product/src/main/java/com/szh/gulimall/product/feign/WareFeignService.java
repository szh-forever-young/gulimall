package com.szh.gulimall.product.feign;

import com.szh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
@FeignClient("gulimall-ware")
public interface WareFeignService {

    @PostMapping("/ware/waresku/hasSkuStock")
    R getHasSkuStock(@RequestBody List<Long> skuIds);
}
