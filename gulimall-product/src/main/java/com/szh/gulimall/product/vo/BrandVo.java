package com.szh.gulimall.product.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2022/11/12
 */
@Data
public class BrandVo {
    private Long brandId;
    private String brandName;
}
