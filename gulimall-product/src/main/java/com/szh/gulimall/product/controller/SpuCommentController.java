package com.szh.gulimall.product.controller;

import java.util.Arrays;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.product.entity.SpuCommentEntity;
import com.szh.gulimall.product.service.SpuCommentService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 商品评价
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@RefreshScope
@RestController
@RequestMapping("/product/spucomment")
public class SpuCommentController {

    @Autowired
    private SpuCommentService spuCommentService;

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = spuCommentService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
		SpuCommentEntity spuComment = spuCommentService.getById(id);
        return R.ok().put("spuComment", spuComment);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody SpuCommentEntity spuComment) {
		spuCommentService.save(spuComment);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody SpuCommentEntity spuComment) {
		spuCommentService.updateById(spuComment);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
		spuCommentService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }
}
