package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.SkuInfoEntity;
import com.szh.gulimall.product.vo.SkuItemVo;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * sku信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface SkuInfoService extends IService<SkuInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //6.1 保存sku的基本信息 pms_sku_info
    void saveSkuInfo(List<SkuInfoEntity> skuInfoEntityList);

    /**
     * 条件查询sku列表
     */
    PageUtils queryPageByCondition(Map<String, Object> params);

    /**
     * 查询当前spuId对应的所有sku信息
     */
    List<SkuInfoEntity> getSkusBySpuId(Long spuId);

    /**
     * 详情页：展示当前sku的详细信息
     */
    SkuItemVo findSkuItemDetail(Long skuId) throws ExecutionException, InterruptedException;
}

