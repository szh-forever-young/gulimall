package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.SkuImagesEntity;

import java.util.List;
import java.util.Map;

/**
 * sku图片
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface SkuImagesService extends IService<SkuImagesEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //6.2 保存sku的图片集 pms_sku_images
    void saveSkuImages(List<SkuImagesEntity> skuImagesEntityList);

    /**
     * 根据skuId查询当前sku的所有图片信息
     */
    List<SkuImagesEntity> getImagesBySkuId(Long skuId);
}

