package com.szh.gulimall.product.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.Query;
import com.szh.gulimall.product.dao.SpuImagesDao;
import com.szh.gulimall.product.entity.SpuImagesEntity;
import com.szh.gulimall.product.service.SpuImagesService;
import org.springframework.util.CollectionUtils;

@Service("spuImagesService")
public class SpuImagesServiceImpl extends ServiceImpl<SpuImagesDao, SpuImagesEntity> implements SpuImagesService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SpuImagesServiceImpl.class);

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SpuImagesEntity> page = this.page(
                new Query<SpuImagesEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    @Override
    public void saveSpuImages(Long id, List<String> imagesList) {
        if (!CollectionUtils.isEmpty(imagesList)) {
            List<SpuImagesEntity> imagesEntityList = imagesList.stream().map(image -> {
                SpuImagesEntity spuImagesEntity = new SpuImagesEntity();
                spuImagesEntity.setSpuId(id);
                spuImagesEntity.setImgUrl(image);
                return spuImagesEntity;
            }).collect(Collectors.toList());
            this.saveBatch(imagesEntityList);
        } else {
            LOGGER.error("当前spuId：{} 下的imagesList为空，{}", id, imagesList);
        }
    }
}