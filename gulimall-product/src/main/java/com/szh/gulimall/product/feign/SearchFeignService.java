package com.szh.gulimall.product.feign;

import com.szh.common.dto.es.SkuEsModel;
import com.szh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/12/18
 */
@FeignClient("gulimall-search")
public interface SearchFeignService {

    @PostMapping("/search/productUp")
    R productUp(@RequestBody List<SkuEsModel> skuEsModels);
}
