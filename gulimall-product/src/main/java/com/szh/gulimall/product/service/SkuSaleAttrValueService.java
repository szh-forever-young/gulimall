package com.szh.gulimall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.szh.gulimall.product.vo.SkuItemSaleAttrVo;

import java.util.List;
import java.util.Map;

/**
 * sku销售属性&值
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
public interface SkuSaleAttrValueService extends IService<SkuSaleAttrValueEntity> {

    PageUtils queryPage(Map<String, Object> params);

    //6.3 保存sku的销售属性 pms_sku_sale_attr_value
    void saveSaleAttrValue(List<SkuSaleAttrValueEntity> skuSaleAttrValueEntityList);

    /**
     * 根据spuId查询所有sku中包括的那些销售属性信息（属性id、属性名、属性值）
     */
    List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(Long spuId);

    /**
     * 查询sku的销售属性组合信息
     */
    List<String> getSkuSaleAttrValues(Long skuId);
}

