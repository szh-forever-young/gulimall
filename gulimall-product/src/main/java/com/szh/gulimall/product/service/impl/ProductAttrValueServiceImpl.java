package com.szh.gulimall.product.service.impl;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.Query;
import com.szh.gulimall.product.dao.ProductAttrValueDao;
import com.szh.gulimall.product.entity.ProductAttrValueEntity;
import com.szh.gulimall.product.service.ProductAttrValueService;
import org.springframework.transaction.annotation.Transactional;

@Service("productAttrValueService")
public class ProductAttrValueServiceImpl extends ServiceImpl<ProductAttrValueDao, ProductAttrValueEntity> implements ProductAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<ProductAttrValueEntity> page = this.page(
                new Query<ProductAttrValueEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    @Override
    public void saveProductAttrValue(List<ProductAttrValueEntity> attrValueEntityList) {
        this.saveBatch(attrValueEntityList);
    }

    /**
     * 查询某个spu商品的所有基本属性/规格参数
     */
    @Override
    public List<ProductAttrValueEntity> baseAttrListForSpu(Long spuId) {
        QueryWrapper<ProductAttrValueEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("spu_id", spuId);
        List<ProductAttrValueEntity> valueEntityList = baseMapper.selectList(queryWrapper);
        return valueEntityList;
    }

    /**
     * 批量更新当前spu商品下的所有基本属性/规格参数
     */
    @Override
    @Transactional
    public void updateSpuAttrs(Long spuId, List<ProductAttrValueEntity> entityList) {
        //1.先删除当前spu商品下对应的所有基本属性
        QueryWrapper<ProductAttrValueEntity> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("spu_id", spuId);
        baseMapper.delete(queryWrapper);
        //2.再将当前spu商品下要更新的基本属性信息插入数据库，
        //由于是新增操作，所以需要为每个基本属性设置好对应的spuId，这里它们的spuId是一样的，因为是同一个商品下的
        List<ProductAttrValueEntity> valueEntityList = entityList.stream().map(item -> {
            item.setSpuId(spuId);
            return item;
        }).collect(Collectors.toList());
        this.saveBatch(valueEntityList);
    }
}