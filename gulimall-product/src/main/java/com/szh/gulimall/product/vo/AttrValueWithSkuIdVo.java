package com.szh.gulimall.product.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/1/4
 */
@Data
public class AttrValueWithSkuIdVo {
    private String attrValue;
    private String skuIds;
}
