package com.szh.gulimall.product.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2022/11/12
 */
@Data
public class Attr {
    private Long attrId;
    private String attrName;
    private String attrValue;
}