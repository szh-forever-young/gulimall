package com.szh.gulimall.product.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.szh.gulimall.product.entity.ProductAttrValueEntity;
import com.szh.gulimall.product.service.ProductAttrValueService;
import com.szh.gulimall.product.vo.AttrRespVo;
import com.szh.gulimall.product.vo.AttrVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.product.service.AttrService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 商品属性
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 14:47:31
 */
@RefreshScope
@RestController
@RequestMapping("/product/attr")
public class AttrController {

    @Autowired
    private AttrService attrService;

    @Autowired
    private ProductAttrValueService productAttrValueService;

    /**
     * 查询某个spu商品的所有基本属性/规格参数
     */
    @GetMapping("/base/listforspu/{spuId}")
    public R baseAttrListForSpu(@PathVariable("spuId") Long spuId) {
        List<ProductAttrValueEntity> entityList = productAttrValueService.baseAttrListForSpu(spuId);
        return R.ok().put("data", entityList);
    }

    /**
     * 获取某个分类下的所有属性信息
     * attrType: 属性类型, 0-销售属性, 1-规格参数
     */
    @GetMapping(value = "/{attrType}/list/{catelogId}")
    public R baseAttrList(@RequestParam Map<String, Object> params,
                          @PathVariable("catelogId") Long catelogId,
                          @PathVariable("attrType") String type) {
        PageUtils page = attrService.queryBaseAttrList(params, catelogId, type);
        return R.ok().put("page", page);
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = attrService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 根据属性id查询详细信息，同时设置分组信息、分类全路径
     */
    @GetMapping("/info/{attrId}")
    public R info(@PathVariable("attrId") Long attrId) {
        AttrRespVo attrRespVo = attrService.queryAttrInfo(attrId);
        return R.ok().put("attr", attrRespVo);
    }

    /**
     * 保存属性信息，同时保存属性分组的关联信息
     */
    @PostMapping("/save")
    public R save(@RequestBody AttrVo attr) {
		attrService.saveAttr(attr);
        return R.ok();
    }

    /**
     * 修改属性信息，同时修改属性分组的关联信息
     */
    @PostMapping("/update")
    public R update(@RequestBody AttrVo attr) {
		attrService.updateAttr(attr);
        return R.ok();
    }

    /**
     * 批量更新当前spu商品下的所有基本属性/规格参数
     */
    @PostMapping("/update/{spuId}")
    public R updateSpuAttrs(@PathVariable("spuId") Long spuId,
                            @RequestBody List<ProductAttrValueEntity> entityList) {
        productAttrValueService.updateSpuAttrs(spuId, entityList);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] attrIds) {
		attrService.removeByIds(Arrays.asList(attrIds));
        return R.ok();
    }
}
