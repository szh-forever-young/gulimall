package com.szh.gulimall.product.service.impl;

import com.szh.gulimall.product.vo.SkuItemSaleAttrVo;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.Query;
import com.szh.gulimall.product.dao.SkuSaleAttrValueDao;
import com.szh.gulimall.product.entity.SkuSaleAttrValueEntity;
import com.szh.gulimall.product.service.SkuSaleAttrValueService;

@Service("skuSaleAttrValueService")
public class SkuSaleAttrValueServiceImpl extends ServiceImpl<SkuSaleAttrValueDao, SkuSaleAttrValueEntity> implements SkuSaleAttrValueService {

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuSaleAttrValueEntity> page = this.page(
                new Query<SkuSaleAttrValueEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    @Override
    public void saveSaleAttrValue(List<SkuSaleAttrValueEntity> skuSaleAttrValueEntityList) {
        this.saveBatch(skuSaleAttrValueEntityList);
    }

    /**
     * 根据spuId查询所有sku中包括的那些销售属性信息（属性id、属性名、属性值）
     */
    @Override
    public List<SkuItemSaleAttrVo> getSaleAttrsBySpuId(Long spuId) {
        return baseMapper.getSaleAttrsBySpuId(spuId);
    }

    /**
     * 查询sku的销售属性组合信息
     */
    @Override
    public List<String> getSkuSaleAttrValues(Long skuId) {
        return baseMapper.getSkuSaleAttrValues(skuId);
    }

}