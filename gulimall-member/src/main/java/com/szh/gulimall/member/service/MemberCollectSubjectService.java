package com.szh.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.member.entity.MemberCollectSubjectEntity;

import java.util.Map;

/**
 * 会员收藏的专题活动
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:44:25
 */
public interface MemberCollectSubjectService extends IService<MemberCollectSubjectEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

