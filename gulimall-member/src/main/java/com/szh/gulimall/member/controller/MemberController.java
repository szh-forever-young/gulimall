package com.szh.gulimall.member.controller;

import java.util.Arrays;
import java.util.Map;
import java.util.Objects;

import com.szh.common.constant.GulimallCodeEnum;
import com.szh.gulimall.member.exception.MobileExistException;
import com.szh.gulimall.member.exception.UsernameExistException;
import com.szh.gulimall.member.vo.MemberLoginVo;
import com.szh.gulimall.member.vo.MemberRegisterVo;
import com.szh.gulimall.member.vo.SocialUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.member.entity.MemberEntity;
import com.szh.gulimall.member.service.MemberService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 会员
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:44:25
 */
@RefreshScope
@RestController
@RequestMapping("/member/member")
public class MemberController {

    @Autowired
    private MemberService memberService;

    /**
     * 注册
     */
    @PostMapping(value = "/register")
    public R register(@RequestBody MemberRegisterVo memberRegisterVo) {
        try {
            memberService.register(memberRegisterVo);
        } catch (MobileExistException ex) {
            return R.error(GulimallCodeEnum.MOBILE_EXIST_EXCEPTION.getCode(), GulimallCodeEnum.MOBILE_EXIST_EXCEPTION.getMsg());
        } catch (UsernameExistException ex) {
            return R.error(GulimallCodeEnum.USERNAME_EXIST_EXCEPTION.getCode(), GulimallCodeEnum.USERNAME_EXIST_EXCEPTION.getMsg());
        }
        return R.ok();
    }

    /**
     * 普通登录
     */
    @PostMapping(value = "/login")
    public R login(@RequestBody MemberLoginVo memberLoginVo) {
        MemberEntity memberEntity = memberService.login(memberLoginVo);
        if (Objects.nonNull(memberEntity)) {
            return R.ok().setData(memberEntity);
        } else {
            return R.error(GulimallCodeEnum.LOGIN_ACCOUNT_PASSWORD_EXCEPTION.getCode(), GulimallCodeEnum.LOGIN_ACCOUNT_PASSWORD_EXCEPTION.getMsg());
        }
    }

    /**
     * Gitee登录或注册
     */
    @PostMapping(value = "/oauth2/login")
    public R oauthLogin(@RequestBody SocialUser socialUser) throws Exception {
        MemberEntity memberEntity = memberService.oauthLogin(socialUser);
        if (Objects.nonNull(memberEntity)) {
            return R.ok().setData(memberEntity);
        } else {
            return R.error(GulimallCodeEnum.GITEE_LOGIN_EXCEPTION.getCode(), GulimallCodeEnum.GITEE_LOGIN_EXCEPTION.getMsg());
        }
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = memberService.queryPage(params);
        return R.ok().put("page", page);
    }


    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
		MemberEntity member = memberService.getById(id);
        return R.ok().put("member", member);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody MemberEntity member) {
		memberService.save(member);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody MemberEntity member) {
		memberService.updateById(member);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
		memberService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }
}
