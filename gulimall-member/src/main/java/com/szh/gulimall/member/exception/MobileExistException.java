package com.szh.gulimall.member.exception;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
public class MobileExistException extends RuntimeException {
    public MobileExistException() {
        super("手机号已存在");
    }
}
