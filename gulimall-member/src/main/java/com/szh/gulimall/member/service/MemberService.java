package com.szh.gulimall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.member.entity.MemberEntity;
import com.szh.gulimall.member.exception.MobileExistException;
import com.szh.gulimall.member.exception.UsernameExistException;
import com.szh.gulimall.member.vo.MemberLoginVo;
import com.szh.gulimall.member.vo.MemberRegisterVo;
import com.szh.gulimall.member.vo.SocialUser;

import java.util.Map;

/**
 * 会员
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:44:25
 */
public interface MemberService extends IService<MemberEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 注册
     */
    void register(MemberRegisterVo memberRegisterVo);

    /**
     * 检查手机号是否唯一
     */
    void checkMobileUnique(String mobile) throws MobileExistException;

    /**
     * 检查用户名是否唯一
     */
    void checkUsernameUnique(String username) throws UsernameExistException;

    /**
     * 登录
     */
    MemberEntity login(MemberLoginVo memberLoginVo);

    /**
     * Gitee登录或注册
     */
    MemberEntity oauthLogin(SocialUser socialUser) throws Exception;
}

