package com.szh.gulimall.member.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@Data
public class MemberLoginVo {
    private String loginAccount;
    private String password;
}
