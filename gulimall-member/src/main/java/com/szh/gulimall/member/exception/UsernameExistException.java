package com.szh.gulimall.member.exception;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
public class UsernameExistException extends RuntimeException {
    public UsernameExistException() {
        super("用户名已存在");
    }
}
