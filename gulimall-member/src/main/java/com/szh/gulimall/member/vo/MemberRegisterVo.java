package com.szh.gulimall.member.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@Data
public class MemberRegisterVo {
    private String userName;
    private String password;
    private String mobile;
}
