package com.szh.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.dto.SkuReductionDto;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.coupon.entity.SkuFullReductionEntity;

import java.util.Map;

/**
 * 商品满减信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:39:02
 */
public interface SkuFullReductionService extends IService<SkuFullReductionEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 6.4 保存sku的优惠、满减等信息 sms_sku_ladder sms_sku_full_reduction sms_member_price
     */
    void saveSkuReduction(SkuReductionDto skuReductionDto);
}

