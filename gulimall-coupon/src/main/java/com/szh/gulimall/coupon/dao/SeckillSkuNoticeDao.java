package com.szh.gulimall.coupon.dao;

import com.szh.gulimall.coupon.entity.SeckillSkuNoticeEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀商品通知订阅
 * 
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:39:02
 */
@Mapper
public interface SeckillSkuNoticeDao extends BaseMapper<SeckillSkuNoticeEntity> {
	
}
