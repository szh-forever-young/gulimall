package com.szh.gulimall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.coupon.entity.SeckillPromotionEntity;

import java.util.Map;

/**
 * 秒杀活动
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:39:02
 */
public interface SeckillPromotionService extends IService<SeckillPromotionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

