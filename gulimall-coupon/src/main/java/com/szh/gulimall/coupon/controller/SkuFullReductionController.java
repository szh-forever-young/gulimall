package com.szh.gulimall.coupon.controller;

import java.util.Arrays;
import java.util.Map;

import com.szh.common.dto.SkuReductionDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.*;

import com.szh.gulimall.coupon.entity.SkuFullReductionEntity;
import com.szh.gulimall.coupon.service.SkuFullReductionService;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.R;

/**
 * 商品满减信息
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:39:02
 */
@RefreshScope
@RestController
@RequestMapping("/coupon/skufullreduction")
public class SkuFullReductionController {

    @Autowired
    private SkuFullReductionService skuFullReductionService;

    /**
     * 6.4 保存sku的优惠、满减等信息 sms_sku_ladder sms_sku_full_reduction sms_member_price
     */
    @PostMapping("/saveSkuReduction")
    public R saveSkuReduction(@RequestBody SkuReductionDto skuReductionDto) {
        skuFullReductionService.saveSkuReduction(skuReductionDto);
        return R.ok();
    }

    /**
     * 列表
     */
    @GetMapping("/list")
    public R list(@RequestParam Map<String, Object> params) {
        PageUtils page = skuFullReductionService.queryPage(params);
        return R.ok().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    public R info(@PathVariable("id") Long id) {
		SkuFullReductionEntity skuFullReduction = skuFullReductionService.getById(id);
        return R.ok().put("skuFullReduction", skuFullReduction);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    public R save(@RequestBody SkuFullReductionEntity skuFullReduction) {
		skuFullReductionService.save(skuFullReduction);
        return R.ok();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    public R update(@RequestBody SkuFullReductionEntity skuFullReduction) {
		skuFullReductionService.updateById(skuFullReduction);
        return R.ok();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    public R delete(@RequestBody Long[] ids) {
		skuFullReductionService.removeByIds(Arrays.asList(ids));
        return R.ok();
    }
}
