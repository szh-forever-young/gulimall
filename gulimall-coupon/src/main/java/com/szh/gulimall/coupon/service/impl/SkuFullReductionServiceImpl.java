package com.szh.gulimall.coupon.service.impl;

import com.szh.common.dto.MemberPrice;
import com.szh.common.dto.SkuReductionDto;
import com.szh.gulimall.coupon.entity.MemberPriceEntity;
import com.szh.gulimall.coupon.entity.SkuLadderEntity;
import com.szh.gulimall.coupon.service.MemberPriceService;
import com.szh.gulimall.coupon.service.SkuLadderService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.szh.common.utils.PageUtils;
import com.szh.common.utils.Query;
import com.szh.gulimall.coupon.dao.SkuFullReductionDao;
import com.szh.gulimall.coupon.entity.SkuFullReductionEntity;
import com.szh.gulimall.coupon.service.SkuFullReductionService;

@Service("skuFullReductionService")
public class SkuFullReductionServiceImpl extends ServiceImpl<SkuFullReductionDao, SkuFullReductionEntity> implements SkuFullReductionService {

    @Autowired
    private SkuLadderService skuLadderService;

    @Autowired
    private MemberPriceService memberPriceService;

    @Override
    public PageUtils queryPage(Map<String, Object> params) {
        IPage<SkuFullReductionEntity> page = this.page(
                new Query<SkuFullReductionEntity>().getPage(params),
                new QueryWrapper<>()
        );
        return new PageUtils(page);
    }

    /**
     * 6.4 保存sku的优惠、满减等信息 sms_sku_ladder sms_sku_full_reduction sms_member_price
     */
    @Override
    public void saveSkuReduction(SkuReductionDto skuReductionDto) {
        //sms_sku_ladder
        SkuLadderEntity skuLadderEntity = new SkuLadderEntity();
        skuLadderEntity.setSkuId(skuReductionDto.getSkuId());
        skuLadderEntity.setFullCount(skuReductionDto.getFullCount());
        skuLadderEntity.setDiscount(skuReductionDto.getDiscount());
        skuLadderEntity.setAddOther(skuLadderEntity.getAddOther());
        if (skuLadderEntity.getFullCount() > 0) {
            skuLadderService.save(skuLadderEntity);
        }
        //sms_sku_full_reduction
        SkuFullReductionEntity skuFullReductionEntity = new SkuFullReductionEntity();
        BeanUtils.copyProperties(skuReductionDto, skuFullReductionEntity);
        if (skuFullReductionEntity.getFullPrice().compareTo(new BigDecimal("0")) > 0) {
            this.save(skuFullReductionEntity);
        }
        //sms_member_price
        List<MemberPrice> memberPriceList = skuReductionDto.getMemberPrice();
        List<MemberPriceEntity> memberPriceEntities = memberPriceList.stream().map(item -> {
            MemberPriceEntity memberPriceEntity = new MemberPriceEntity();
            memberPriceEntity.setSkuId(skuReductionDto.getSkuId());
            memberPriceEntity.setMemberLevelId(item.getId());
            memberPriceEntity.setMemberLevelName(item.getName());
            memberPriceEntity.setMemberPrice(item.getPrice());
            memberPriceEntity.setAddOther(1);
            return memberPriceEntity;
        }).filter(item -> item.getMemberPrice().compareTo(new BigDecimal("0")) > 0).collect(Collectors.toList());
        memberPriceService.saveBatch(memberPriceEntities);
    }
}