package com.szh.gulimall.cart.controller;

import com.szh.gulimall.cart.service.CartService;
import com.szh.gulimall.cart.vo.Cart;
import com.szh.gulimall.cart.vo.CartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author: SongZiHao
 * @date: 2023/1/16
 */
@Controller
public class CartController {

    @Autowired
    private CartService cartService;

    /**
     * 获取当前用户所有选中的购物项
     */
    @ResponseBody
    @GetMapping("/currentUserCartItems")
    public List<CartItem> getCurrentUserCartItems() {
        return cartService.getCurrentUserCartItems();
    }

    /**
     * 获取整个购物车数据 : 如果登录了，就获取用户自己的在线购物车 + 登录之前的临时购物车数据；
     *                   如果没登录，则只需获取临时购物车数据
     */
    @GetMapping(value = "/cart.html")
    public String cartListPage(Model model) throws ExecutionException, InterruptedException {
        Cart cart = cartService.getCart();
        model.addAttribute("cart", cart);
        return "cartList";
    }

    /**
     * 添加商品到购物车
     * RedirectAttributes redirectAttributes
     * redirectAttributes.addFlashAttribute() : 将数据存在session中，可以从页面中取出，但只能取一次
     * redirectAttributes.addAttribute() : 将数据放在url后面，可以从url中取出
     */
    @GetMapping(value = "/addToCart")
    public String addToCart(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num,
                            RedirectAttributes redirectAttributes) throws ExecutionException, InterruptedException {
        CartItem cartItem = cartService.addToCart(skuId, num);
        redirectAttributes.addAttribute("skuId", skuId);
        //重定向到成功页面
        return "redirect:http://cart.gulimall.com/addToCartSuccess.html";
    }

    /**
     * 获取购物车中的某个购物项，也就是成功添加购物项到购物车之后的成功页面
     */
    @GetMapping(value = "/addToCartSuccess.html")
    public String addToCartSuccess(@RequestParam("skuId") Long skuId, Model model) {
        //再次查询购物车数据即可
        CartItem cartItem = cartService.getCartItem(skuId);
        model.addAttribute("item", cartItem);
        return "success";
    }

    /**
     * 选中购物车中的某个购物项，选中之后，重定向到购物车列表页面
     */
    @GetMapping(value = "/checkItem")
    public String checkItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("check") Integer check) {
        cartService.checkItem(skuId, check);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    /**
     * 改变购物车中某个购物项的数量，改变之后，重定向到购物车列表页面
     */
    @GetMapping(value = "/countItem")
    public String countItem(@RequestParam("skuId") Long skuId,
                            @RequestParam("num") Integer num) {
        cartService.countItem(skuId, num);
        return "redirect:http://cart.gulimall.com/cart.html";
    }

    /**
     * 删除购物车中某个购物项，改变之后，重定向到购物车列表页面
     */
    @GetMapping(value = "/deleteItem")
    public String deleteItem(@RequestParam("skuId") Long skuId) {
        cartService.deleteItem(skuId);
        return "redirect:http://cart.gulimall.com/cart.html";
    }
}
