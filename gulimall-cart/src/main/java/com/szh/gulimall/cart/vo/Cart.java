package com.szh.gulimall.cart.vo;

import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;

/**
 * 整个购物车的数据结构
 *
 * @author: SongZiHao
 * @date: 2023/1/16
 */
public class Cart {
    /**
     * 购物车中的所有购物项
     */
    private List<CartItem> items;
    /**
     * 购物项的数量
     */
    private Integer countNum;
    /**
     * 购物项的种类
     */
    private Integer countType;
    /**
     * 购物项的总价
     */
    private BigDecimal totalAmount;
    /**
     * 购物项的优惠价
     */
    private BigDecimal reduce = new BigDecimal("0.00");

    public List<CartItem> getItems() {
        return items;
    }

    public void setItems(List<CartItem> items) {
        this.items = items;
    }

    public Integer getCountNum() {
        int count = 0;
        if (!CollectionUtils.isEmpty(items)) {
            for (CartItem item : items) {
                count += item.getCount();
            }
        }
        return count;
    }

    public Integer getCountType() {
        int count = 0;
        if (!CollectionUtils.isEmpty(items)) {
            for (CartItem item : items) {
                count += 1;
            }
        }
        return count;
    }

    public BigDecimal getTotalAmount() {
        BigDecimal amount = new BigDecimal("0");
        //1.计算购物项总价
        if (!CollectionUtils.isEmpty(items)) {
            for (CartItem item : items) {
                if (item.getCheck()) {
                    BigDecimal price = item.getTotalPrice();
                    amount = amount.add(price);
                }
            }
        }
        //2.减去优惠总结
        BigDecimal result = amount.subtract(getReduce());
        return result;
    }

    public BigDecimal getReduce() {
        return reduce;
    }

    public void setReduce(BigDecimal reduce) {
        this.reduce = reduce;
    }
}
