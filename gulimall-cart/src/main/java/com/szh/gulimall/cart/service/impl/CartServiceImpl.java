package com.szh.gulimall.cart.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.szh.common.utils.R;
import com.szh.gulimall.cart.feign.ProductFeignService;
import com.szh.gulimall.cart.interceptor.CartInterceptor;
import com.szh.gulimall.cart.service.CartService;
import com.szh.gulimall.cart.vo.Cart;
import com.szh.gulimall.cart.vo.CartItem;
import com.szh.gulimall.cart.vo.SkuInfoVo;
import com.szh.gulimall.cart.vo.UserInfoTo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.stream.Collectors;

/**
 * @author: SongZiHao
 * @date: 2023/1/16
 */
@Service
public class CartServiceImpl implements CartService {

    private static final Logger LOGGER = LoggerFactory.getLogger(CartServiceImpl.class);

    private final String CART_PREFIX = "gulimall:cart:";

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Autowired
    private ProductFeignService productFeignService;

    @Autowired
    private ThreadPoolExecutor threadPoolExecutor;

    /**
     * 添加商品到购物车
     */
    @Override
    public CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException {
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        //添加商品到购物车之前，先看一下redis中有没有该商品
        String res = (String) cartOps.get(String.valueOf(skuId));
        if (StringUtils.isEmpty(res)) { //如果没有，正常添加
            CartItem cartItem = new CartItem();
            //添加新的商品到购物车中
            CompletableFuture<Void> getSkuInfoTask = CompletableFuture.runAsync(() -> {
                //1.远程调用商品服务，根据skuId查询商品信息
                R result = productFeignService.getSkuInfo(skuId);
                SkuInfoVo skuInfo = result.getData("skuInfo", new TypeReference<SkuInfoVo>() {});
                //2.将获取到的商品信息添加到购物车
                cartItem.setCount(num);
                cartItem.setCheck(true);
                cartItem.setSkuId(skuId);
                cartItem.setImage(skuInfo.getSkuDefaultImg());
                cartItem.setTitle(skuInfo.getSkuTitle());
                cartItem.setPrice(skuInfo.getPrice());
            }, threadPoolExecutor);
            CompletableFuture<Void> getSkuSaleAttrValuesTask = CompletableFuture.runAsync(() -> {
                //3.远程调用商品服务，查询sku的销售属性组合信息
                List<String> skuSaleAttrValues = productFeignService.getSkuSaleAttrValues(skuId);
                cartItem.setSkuAttr(skuSaleAttrValues);
            }, threadPoolExecutor);
            //等待以上两个异步任务都完成，代码才会继续向下执行
            CompletableFuture.allOf(getSkuInfoTask, getSkuSaleAttrValuesTask).get();
            String jsonString = JSON.toJSONString(cartItem);
            cartOps.put(String.valueOf(skuId), jsonString);
            return cartItem;
        } else { //如果有，则从redis中拿到该商品，修改商品数量信息
            CartItem cartItem = JSON.parseObject(res, CartItem.class);
            cartItem.setCount(cartItem.getCount() + num);
            //修改完商品数量之后，重新把购物项信息存入redis
            cartOps.put(String.valueOf(skuId), JSON.toJSONString(cartItem));
            return cartItem;
        }
    }

    /**
     * 获取购物车中的某个购物项
     */
    @Override
    public CartItem getCartItem(Long skuId) {
        //从redis获取到要操作的购物车
        BoundHashOperations<String, Object, Object> cartOps = getCartOps();
        String str = (String) cartOps.get(String.valueOf(skuId));
        CartItem cartItem = JSON.parseObject(str, CartItem.class);
        return cartItem;
    }

    /**
     * 获取整个购物车数据 : 如果登录了，就获取用户自己的在线购物车 + 登录之前的临时购物车数据；
     *                   如果没登录，则只需获取临时购物车数据
     */
    @Override
    public Cart getCart() throws ExecutionException, InterruptedException {
        //从ThreadLocal中获取用户登录信息
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        Cart cart = new Cart();
        if (Objects.nonNull(userInfoTo.getUserId())) { //用户登录了，就获取用户自己的在线购物车 + 登录之前的临时购物车数据；
            String cartKey = CART_PREFIX + userInfoTo.getUserId(); //在线购物车的key
            String tempCartKey = CART_PREFIX + userInfoTo.getUserKey(); //临时购物车的key
            //如果临时购物车中还有数据，则先获取临时购物车数据，将临时购物车数据合并到在线购物车中
            BoundHashOperations<String, Object, Object> hashOperations = redisTemplate.boundHashOps(tempCartKey);
            List<Object> values = hashOperations.values();
            if (!CollectionUtils.isEmpty(values)) {
                List<CartItem> cartItems = values.stream().map(obj -> {
                    String str = (String) obj;
                    CartItem cartItem = JSON.parseObject(str, CartItem.class);
                    return cartItem;
                }).collect(Collectors.toList());
                if (!CollectionUtils.isEmpty(cartItems)) {
                    //将临时购物车中的每个购物项合并到用户自己的在线购物车中
                    for (CartItem cartItem : cartItems) {
                        addToCart(cartItem.getSkuId(), cartItem.getCount());
                    }
                    //合并之后，将临时购物车数据清空
                    clearCart(tempCartKey);
                }
            }
            //最后获取合并之后的在线购物车数据
            BoundHashOperations<String, Object, Object> hashOps = redisTemplate.boundHashOps(cartKey);
            List<Object> objectList = hashOps.values();
            if (!CollectionUtils.isEmpty(objectList)) {
                List<CartItem> cartItems = objectList.stream().map(obj -> {
                    String str = (String) obj;
                    CartItem cartItem = JSON.parseObject(str, CartItem.class);
                    return cartItem;
                }).collect(Collectors.toList());
                cart.setItems(cartItems);
            }
        } else { //没登录，则只需获取临时购物车数据
            String cartKey = CART_PREFIX + userInfoTo.getUserKey();
            BoundHashOperations<String, Object, Object> hashOperations = redisTemplate.boundHashOps(cartKey);
            List<Object> values = hashOperations.values();
            if (!CollectionUtils.isEmpty(values)) {
                List<CartItem> cartItems = values.stream().map(obj -> {
                    String str = (String) obj;
                    CartItem cartItem = JSON.parseObject(str, CartItem.class);
                    return cartItem;
                }).collect(Collectors.toList());
                cart.setItems(cartItems);
            }
        }
        return cart;
    }

    /**
     * 清空购物车数据
     */
    @Override
    public void clearCart(String cartKey) {
        redisTemplate.delete(cartKey);
    }

    /**
     * 选中购物车中的某个购物项，选中之后，重定向到购物车列表页面
     */
    @Override
    public void checkItem(Long skuId, Integer check) {
        //首先获取到这个购物项，将该购物项的状态修改为已选中
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCheck(check == 1);
        //从redis获取到要操作的购物车
        BoundHashOperations<String, Object, Object> hashOperations = getCartOps();
        String jsonStr = JSON.toJSONString(cartItem);
        //再将该购物项重新存入redis
        hashOperations.put(String.valueOf(skuId), jsonStr);
    }

    /**
     * 改变购物车中某个购物项的数量，改变之后，重定向到购物车列表页面
     */
    @Override
    public void countItem(Long skuId, Integer num) {
        //首先获取到这个购物项，将该购物项的数量修改为num
        CartItem cartItem = getCartItem(skuId);
        cartItem.setCount(num);
        //从redis获取到要操作的购物车
        BoundHashOperations<String, Object, Object> hashOperations = getCartOps();
        String jsonStr = JSON.toJSONString(cartItem);
        //再将该购物项重新存入redis
        hashOperations.put(String.valueOf(skuId), jsonStr);
    }

    /**
     * 删除购物车中某个购物项，改变之后，重定向到购物车列表页面
     */
    @Override
    public void deleteItem(Long skuId) {
        //从redis获取到要操作的购物车，根据skuId删除这个购物项
        BoundHashOperations<String, Object, Object> hashOperations = getCartOps();
        hashOperations.delete(String.valueOf(skuId));
    }

    /**
     * 获取当前用户所有选中的购物项
     */
    @Override
    public List<CartItem> getCurrentUserCartItems() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        if (userInfoTo.getUserId() == null) {
            return null;
        } else {
            String cartKey = CART_PREFIX + userInfoTo.getUserId();
            List<CartItem> cartItems = getCartItems(cartKey);
            List<CartItem> itemList = cartItems.stream()
                    .filter(item -> item.getCheck())
                    .map(item -> {
                        R res = productFeignService.getPriceBySkuId(item.getSkuId());
                        String price = (String) res.get("data");
                        item.setPrice(new BigDecimal(price));
                        return item;
                    })
                    .collect(Collectors.toList());
            return itemList;
        }
    }

    /**
     * 获取购物车里面的数据
     * @param cartKey
     * @return
     */
    private List<CartItem> getCartItems(String cartKey) {
        //获取购物车里面的所有商品
        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);
        List<Object> values = operations.values();
        if (values != null && values.size() > 0) {
            List<CartItem> cartItemList = values.stream().map((obj) -> {
                String str = (String) obj;
                CartItem cartItem = JSON.parseObject(str, CartItem.class);
                return cartItem;
            }).collect(Collectors.toList());
            return cartItemList;
        }
        return null;
    }

    /**
     * 获取到要操作的购物车
     */
    private BoundHashOperations<String, Object, Object> getCartOps() {
        UserInfoTo userInfoTo = CartInterceptor.threadLocal.get();
        String cartKey = "";
        if (userInfoTo.getUserId() != null) {
            cartKey = CART_PREFIX + userInfoTo.getUserId();
        } else {
            cartKey = CART_PREFIX + userInfoTo.getUserKey();
        }
        BoundHashOperations<String, Object, Object> operations = redisTemplate.boundHashOps(cartKey);
        return operations;
    }
}
