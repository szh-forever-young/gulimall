package com.szh.gulimall.cart.service;

import com.szh.gulimall.cart.vo.Cart;
import com.szh.gulimall.cart.vo.CartItem;

import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * @author: SongZiHao
 * @date: 2023/1/16
 */
public interface CartService {

    /**
     * 添加商品到购物车
     */
    CartItem addToCart(Long skuId, Integer num) throws ExecutionException, InterruptedException;

    /**
     * 获取购物车中的某个购物项
     */
    CartItem getCartItem(Long skuId);

    /**
     * 获取整个购物车数据 : 如果登录了，就获取用户自己的在线购物车 + 登录之前的临时购物车数据；
     *                   如果没登录，则只需获取临时购物车数据
     */
    Cart getCart() throws ExecutionException, InterruptedException;

    /**
     * 清空购物车数据
     */
    void clearCart(String cartKey);

    /**
     * 选中购物车中的某个购物项，选中之后，重定向到购物车列表页面
     */
    void checkItem(Long skuId, Integer check);

    /**
     * 改变购物车中某个购物项的数量，改变之后，重定向到购物车列表页面
     */
    void countItem(Long skuId, Integer num);

    /**
     * 删除购物车中某个购物项，改变之后，重定向到购物车列表页面
     */
    void deleteItem(Long skuId);

    /**
     * 获取当前用户所有选中的购物项
     */
    List<CartItem> getCurrentUserCartItems();
}
