package com.szh.gulimall.cart.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/1/16
 */
@Data
public class UserInfoTo {
    private Long userId;
    private String userKey;
    private boolean tempUser = false;
}
