package com.szh.gulimall.cart.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @author: SongZiHao
 * @date: 2023/1/4
 */
@Data
@Component
@ConfigurationProperties(prefix = "gulimall.thread.pool")
public class ThreadPoolProperties {
    private Integer corePoolSize;
    private Integer maximumPoolSize;
    private Integer keepAliveTime;
}
