package com.szh.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.order.entity.OrderEntity;
import com.szh.gulimall.order.vo.OrderConfirmVo;
import com.szh.gulimall.order.vo.OrderSubmitVo;
import com.szh.gulimall.order.vo.SubmitOrderResponseVo;

import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 * 订单
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:48:51
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);

    /**
     * 跳转到订单结算页，同时封装页面所需的数据进行返回
     */
    OrderConfirmVo confirmOrder() throws ExecutionException, InterruptedException;

    /**
     * 订单下单
     */
    SubmitOrderResponseVo submitOrder(OrderSubmitVo vo);

    /**
     * 根据订单号查询订单状态
     */
    OrderEntity getOrderStatus(String orderSn);

    void closeOrder(OrderEntity orderEntity);
}

