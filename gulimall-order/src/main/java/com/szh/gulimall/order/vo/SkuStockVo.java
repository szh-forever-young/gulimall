package com.szh.gulimall.order.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/2/3
 */
@Data
public class SkuStockVo {
    private Long skuId;
    private Boolean hasStock;
}
