package com.szh.gulimall.order.to;

import com.szh.gulimall.order.entity.OrderEntity;
import com.szh.gulimall.order.entity.OrderItemEntity;
import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
@Data
public class OrderCreateTo {
    private OrderEntity order;
    private List<OrderItemEntity> orderItems;
    private BigDecimal payPrice;
    private BigDecimal fare;
}
