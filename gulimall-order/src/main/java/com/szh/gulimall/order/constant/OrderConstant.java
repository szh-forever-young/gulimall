package com.szh.gulimall.order.constant;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
public class OrderConstant {

    public static final String USER_ORDER_TOKEN_PREFIX = "order:token:";

}
