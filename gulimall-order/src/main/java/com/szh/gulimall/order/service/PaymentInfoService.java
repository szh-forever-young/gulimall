package com.szh.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.order.entity.PaymentInfoEntity;

import java.util.Map;

/**
 * 支付信息表
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:48:51
 */
public interface PaymentInfoService extends IService<PaymentInfoEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

