package com.szh.gulimall.order.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
@Data
public class FareVo {
    private MemberAddressVo address;
    private BigDecimal fare;
}
