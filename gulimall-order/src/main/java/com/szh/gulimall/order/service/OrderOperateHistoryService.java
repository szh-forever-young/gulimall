package com.szh.gulimall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.szh.common.utils.PageUtils;
import com.szh.gulimall.order.entity.OrderOperateHistoryEntity;

import java.util.Map;

/**
 * 订单操作历史记录
 *
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:48:52
 */
public interface OrderOperateHistoryService extends IService<OrderOperateHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

