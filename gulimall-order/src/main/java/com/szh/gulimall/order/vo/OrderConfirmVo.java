package com.szh.gulimall.order.vo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

/**
 * 订单确认页的数据模型VO类
 *
 * @author: SongZiHao
 * @date: 2023/2/3
 */
public class OrderConfirmVo {

    /**
     * 收货地址列表
     */
    @Getter
    @Setter
    private List<MemberAddressVo> address;

    /**
     * 所有选中的购物项
     */
    @Getter
    @Setter
    private List<OrderItemVo> items;

    /**
     * 优惠劵信息
     */
    @Getter
    @Setter
    private Integer integeration;

    /**
     * 防止重复提交令牌
     */
    @Getter
    @Setter
    private String orderToken;

    /**
     * 每个sku是否有库存的map集合
     */
    @Getter
    @Setter
    private Map<Long, Boolean> stockMap;

    /**
     * 订单总金额
     */
    private BigDecimal total;

    /**
     * 应付总金额
     */
    private BigDecimal payPrice;

    /**
     * 商品总件数
     */
    private Integer count;

    public BigDecimal getTotal() {
        BigDecimal total = new BigDecimal("0");
        if (!CollectionUtils.isEmpty(items)) {
            for (OrderItemVo item : items) {
                BigDecimal res = item.getPrice().multiply(new BigDecimal(String.valueOf(item.getCount())));
                total = total.add(res);
            }
        }
        return total;
    }

    public BigDecimal getPayPrice() {
        return getTotal();
    }

    public Integer getCount() {
        Integer count = 0;
        if (!CollectionUtils.isEmpty(items)) {
            for (OrderItemVo item : items) {
                count += item.getCount();
            }
        }
        return count;
    }
}
