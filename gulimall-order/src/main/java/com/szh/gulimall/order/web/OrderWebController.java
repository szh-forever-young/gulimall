package com.szh.gulimall.order.web;

import com.szh.common.exception.NoStockException;
import com.szh.gulimall.order.service.OrderService;
import com.szh.gulimall.order.vo.OrderConfirmVo;
import com.szh.gulimall.order.vo.OrderSubmitVo;
import com.szh.gulimall.order.vo.SubmitOrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.concurrent.ExecutionException;

/**
 * @author: SongZiHao
 * @date: 2023/2/3
 */
@Controller
public class OrderWebController {

    @Autowired
    private OrderService orderService;

    /**
     * 跳转到订单结算页，同时封装页面所需的数据进行返回
     */
    @GetMapping("/toTrade")
    public String toTrade(Model model) throws ExecutionException, InterruptedException {
        OrderConfirmVo confirmVo = orderService.confirmOrder();
        model.addAttribute("orderConfirmData", confirmVo);
        return "confirm";
    }

    /**
     * 订单下单
     */
    @PostMapping("/submitOrder")
    public String submitOrder(OrderSubmitVo vo, Model model, RedirectAttributes redirectAttributes) {
        try {
            //下单，去创建订单、验令牌、锁库存等
            SubmitOrderResponseVo responseVo = orderService.submitOrder(vo);
            //下单成功跳转到订单支付页
            if (responseVo.getCode() == 0) {
                model.addAttribute("submitOrderResp", responseVo);
                return "pay";
            } else { //下单失败回到订单结算页，重新确认订单信息
                String msg = "下单失败，";
                switch (responseVo.getCode()) {
                    case 1:
                        msg += "订单信息过期，请刷新后再次提交";
                        break;
                    case 2:
                        msg += "订单商品价格发生变化，请确认后再次提交";
                        break;
                    case 3:
                        msg += "库存锁定失败，商品库存不足";
                        break;
                }
                redirectAttributes.addFlashAttribute("msg", msg);
                return "redirect:http://order.gulimall.com/toTrade";
            }
        } catch (Exception ex) {
            if (ex instanceof NoStockException) {
                String message = ex.getMessage();
                redirectAttributes.addFlashAttribute("msg",message);
            }
            return "redirect:http://order.gulimall.com/toTrade";
        }
    }
}
