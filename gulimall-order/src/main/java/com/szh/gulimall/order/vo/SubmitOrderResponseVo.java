package com.szh.gulimall.order.vo;

import com.szh.gulimall.order.entity.OrderEntity;
import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
@Data
public class SubmitOrderResponseVo {
    private OrderEntity order;
    private Integer code;
}
