package com.szh.gulimall.order.dao;

import com.szh.gulimall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author SongZiHao
 * @email 2656307671@qq.com
 * @date 2022-10-30 15:48:51
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
