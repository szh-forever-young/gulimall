package com.szh.gulimall.order;

import com.szh.gulimall.order.entity.OrderReturnReasonEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.AmqpAdmin;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

@SpringBootTest
@RunWith(SpringRunner.class)
public class GulimallOrderApplicationTests {

    @Autowired
    private AmqpAdmin amqpAdmin;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Test
    public void createExchange() {
        DirectExchange directExchange = new DirectExchange("hello.java.exchange",
                true, false);
        amqpAdmin.declareExchange(directExchange);
    }

    @Test
    public void createQueue() {
        Queue queue = new Queue("hello-java-queue",
                true, false, false);
        amqpAdmin.declareQueue(queue);
    }

    @Test
    public void createBinding() {
        Binding binding = new Binding("hello-java-queue",
                Binding.DestinationType.QUEUE,
                "hello.java.exchange",
                "hello.java",
                null);
        amqpAdmin.declareBinding(binding);
    }

    @Test
    public void testSendMessage() {
        String msg = "hello world....";
        OrderReturnReasonEntity entity = new OrderReturnReasonEntity();
        entity.setId(1L);
        entity.setName("退货了");
        entity.setCreateTime(new Date());
        rabbitTemplate.convertAndSend("hello.java.exchange",
                "hello.java", entity);
    }

}
