package com.szh.gulimall.auth.vo;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/1/10
 */
@Data
public class SocialUser {
    private String access_token;
    private String token_type;
    private long expires_in;
    private String refresh_token;
    private String scope;
    private String created_at;
}
