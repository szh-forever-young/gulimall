package com.szh.gulimall.auth.feign;

import com.szh.common.utils.R;
import com.szh.gulimall.auth.vo.SocialUser;
import com.szh.gulimall.auth.vo.UserLoginVo;
import com.szh.gulimall.auth.vo.UserRegisterVo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@FeignClient("gulimall-member")
public interface MemberFeignService {

    @PostMapping(value = "/member/member/register")
    R register(@RequestBody UserRegisterVo userRegisterVo);

    @PostMapping(value = "/member/member/login")
    R login(@RequestBody UserLoginVo userLoginVo);

    @PostMapping(value = "/member/member/oauth2/login")
    R oauthLogin(@RequestBody SocialUser socialUser) throws Exception;
}
