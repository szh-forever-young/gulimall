package com.szh.gulimall.auth.vo;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@Data
public class UserLoginVo {

    @NotEmpty(message = "账号必须提交")
    @Length(min = 6, max = 18, message = "账号必须是6-18位字符")
    private String loginAccount;

    @NotEmpty(message = "密码必须填写")
    @Length(min = 6, max = 18, message = "密码必须是6-18位字符")
    private String password;
}
