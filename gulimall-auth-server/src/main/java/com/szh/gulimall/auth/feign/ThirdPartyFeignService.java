package com.szh.gulimall.auth.feign;

import com.szh.common.utils.R;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@FeignClient("gulimall-third-party")
public interface ThirdPartyFeignService {

    @GetMapping("/sms/sendCode")
    R sendSmsCode(@RequestParam("mobile") String mobile, @RequestParam("code") String code);

}
