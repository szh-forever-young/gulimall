package com.szh.common.exception;

/**
 * @author: SongZiHao
 * @date: 2023/2/4
 */
public class NoStockException extends RuntimeException {
    private Long skuId;

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public NoStockException(Long skuId) {
        super("商品id：" + skuId + "，没有足够的库存了");
    }

    public NoStockException(String msg) {
        super(msg);
    }
}
