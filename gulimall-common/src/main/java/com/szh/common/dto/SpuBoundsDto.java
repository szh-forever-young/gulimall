package com.szh.common.dto;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @author: SongZiHao
 * @date: 2022/11/13
 */
@Data
public class SpuBoundsDto {
    private Long spuId;
    private BigDecimal buyBounds;
    private BigDecimal growBounds;
}
