package com.szh.common.dto.mq;

import lombok.Data;

/**
 * @author: SongZiHao
 * @date: 2023/2/6
 */
@Data
public class StockLockTo {
    private Long id; //库存工作单id
    private StockDetailTo detail; //工作单详情
}
