package com.szh.common.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author: SongZiHao
 * @date: 2022/11/13
 */
@Data
public class SkuReductionDto {
    private Long skuId;
    private int fullCount;
    private BigDecimal discount;
    private int countStatus;
    private BigDecimal fullPrice;
    private BigDecimal reducePrice;
    private int priceStatus;
    private List<MemberPrice> memberPrice;
}
