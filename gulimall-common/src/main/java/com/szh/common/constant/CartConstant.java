package com.szh.common.constant;

/**
 * @author: SongZiHao
 * @date: 2023/1/16
 */
public class CartConstant {

    public static final String TEMP_USER_COOKIE_NAME = "user-key";

    public static final int TEMP_USER_COOKIE_TIMEOUT = 60 * 60 * 24 * 30;
}
