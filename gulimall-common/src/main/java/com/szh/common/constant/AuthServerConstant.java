package com.szh.common.constant;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
public class AuthServerConstant {

    /**
     * 短信验证码在Redis中存储的key前缀部分
     */
    public static final String SMS_CODE_CACHE_PREFIX = "sms:code:";

    /**
     * 登录时，向session中存数据的key
     */
    public static final String SESSION_KEY = "loginUser";
}
