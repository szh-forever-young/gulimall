package com.szh.common.constant;

/**
 * 全局状态码枚举类
 *
 * @author: SongZiHao
 * @date: 2022/11/5
 */
public enum GulimallCodeEnum {
    UNKNOWN_EXCEPTION(10000, "系统未知异常"),
    VALID_EXCEPTION(10001, "参数格式校验失败"),
    SMS_CODE_EXCEPTION(10002, "短信验证码获取频率太高，请稍后再试"),
    PRODUCT_UP_EXCEPTION(11000, "商品上架异常"),
    NO_STOCK_EXCEPTION(12000, "商品库存不足"),
    USERNAME_EXIST_EXCEPTION(15001, "用户名已存在"),
    MOBILE_EXIST_EXCEPTION(15002, "手机号已存在"),
    LOGIN_ACCOUNT_PASSWORD_EXCEPTION(15003, "账号或密码错误"),
    GITEE_LOGIN_EXCEPTION(15004, "Gitee登录失败");

    private int code;
    private String msg;

    GulimallCodeEnum(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
