package com.szh.gulimall.thirdparty.component;

import com.szh.gulimall.thirdparty.util.HttpUtils;
import lombok.Data;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 发送短信验证码组件类，整合阿里云的短信验证码服务
 *
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@Data
@Component
@ConfigurationProperties(prefix = "spring.cloud.alicloud.sms")
public class SendSmsComponent {

    @Value("${spring.cloud.alicloud.sms.host}")
    private String host;

    @Value("${spring.cloud.alicloud.sms.path}")
    private String path;

    @Value("${spring.cloud.alicloud.sms.appcode}")
    private String appcode;

    @Value("${spring.cloud.alicloud.sms.smsSignId}")
    private String smsSignId;

    @Value("${spring.cloud.alicloud.sms.templateId}")
    private String templateId;

    public void sendSmsCode(String mobile, String code) {
//        String host = "https://gyytz.market.alicloudapi.com";
//        String path = "/sms/smsSend";
//        String appcode = "780db0981294479c900ea44cdbe7c024";
        String method = "POST";
        Map<String, String> headers = new HashMap<String, String>();
        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        Map<String, String> querys = new HashMap<String, String>();
        querys.put("mobile", mobile);
        String param = "**code**:" + code + ",**minute**:5";
        querys.put("param", param);
        querys.put("smsSignId", smsSignId);
        querys.put("templateId", templateId);
        Map<String, String> bodys = new HashMap<String, String>();
        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             *
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            System.out.println(response.toString());
            //获取response的body
            System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
