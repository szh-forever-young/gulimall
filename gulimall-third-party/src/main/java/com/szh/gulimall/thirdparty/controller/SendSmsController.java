package com.szh.gulimall.thirdparty.controller;

import com.szh.common.utils.R;
import com.szh.gulimall.thirdparty.component.SendSmsComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: SongZiHao
 * @date: 2023/1/7
 */
@RestController
@RequestMapping(value = "/sms")
public class SendSmsController {

    @Autowired
    private SendSmsComponent sendSmsComponent;

    /**
     * 提供给其他服务进行调用，获取短信验证码
     *
     * @param mobile 手机号
     * @param code 验证码
     * @return R.ok()
     */
    @GetMapping("/sendCode")
    public R sendSmsCode(@RequestParam("mobile") String mobile,
                         @RequestParam("code") String code) {
        sendSmsComponent.sendSmsCode(mobile, code);
        return R.ok();
    }
}
